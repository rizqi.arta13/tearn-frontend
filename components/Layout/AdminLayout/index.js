import AdminNav from 'app/AdminNav';
import Footer from 'components/build/Footer';
import Navbar from 'components/build/Navbar';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { parseJwt } from 'utils/parseJwt';

const AdminLayout = ({ children }) => {
  const router = useRouter();

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt.role.role !== 'admin') {
      router.push('/');
    }
  }, []);
  return (
    <>
      <Navbar logged={true} active={'Beranda'} />
      <div className='py-16 pt-32'>
        <div className='flex justify-center gap-5'>
          <AdminNav />
          {children}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default AdminLayout;
