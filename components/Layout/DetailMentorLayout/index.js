import Footer from 'components/build/Footer';
import Navbar from 'components/build/Navbar';

const DetailMentorLayout = ({ children }) => {
  return (
    <>
      <Navbar logged={true} active={'Beranda'} />
      <div className='py-16 pt-24'>{children}</div>
      <Footer />
    </>
  );
};

export default DetailMentorLayout;
