import SearchMentor from 'app/SearchMentor';
import SearchMentorFilter from 'app/SearchMentorFilter';
import Footer from 'components/build/Footer';
import Navbar from 'components/build/Navbar';
import moment from 'moment';
import { createContext, useState } from 'react';

export const SearchMentorContext = createContext();

const SearchMentorLayout = ({ children }) => {
  const [filter, setFilter] = useState({
    startAge: 18,
    endAge: 100,
    startDate: '',
    endDate: '',
    startTime: '',
    endTime: '',
    gender: '',
    startPrice: 10000,
    endPrice: 2500000,
    subject: '',
    pages: 1,
    limit: 10,
  });

  return (
    <>
      <Navbar logged={false} active={'Cari Mentor'} />
      <div className='pt-28 min-h-[calc(100vh-200px)]'>
        <div className='flex justify-center'>
          <div className='w-[1200px] border p-6 flex flex-col gap-6'>
            <div>
              <SearchMentor
                onFinish={(values) => {
                  let tempFilter = filter;

                  let startDate = moment(values.scheduleDate[0]).format(
                    'YYYY-MM-DD'
                  );
                  let endDate = moment(values.scheduleDate[1]).format(
                    'YYYY-MM-DD'
                  );

                  let startTime = moment(values.scheduleTime[0]).format(
                    'HH:mm'
                  );
                  let endTime = moment(values.scheduleTime[1]).format('HH:mm');

                  tempFilter = {
                    ...tempFilter,
                    startDate,
                    startTime,
                    endDate,
                    endTime,
                    ...values,
                  };
                  setFilter({ ...tempFilter });
                }}
              />
            </div>
            <div className='flex gap-6'>
              <div className='min-w-[200px] w-1/4 bg-light rounded-lg shadow-lg sticky h-min top-28'>
                <SearchMentorFilter
                  onFinish={(values) => {
                    let tempFilter = filter;

                    let startAge = values.age[0];
                    let endAge = values.age[1];
                    let startPrice = values.price[0];
                    let endPrice = values.price[1];

                    tempFilter = {
                      ...tempFilter,
                      startAge,
                      endAge,
                      startPrice,
                      endPrice,
                      ...values,
                    };
                    setFilter({ ...tempFilter });
                  }}
                />
              </div>
              <div className='w-3/4 bg-light rounded-lg shadow-lg min-h-[300px] p-6 flex flex-col gap-4'>
                <SearchMentorContext.Provider value={filter}>
                  {children}
                </SearchMentorContext.Provider>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default SearchMentorLayout;
