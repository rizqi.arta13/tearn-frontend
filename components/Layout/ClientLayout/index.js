import ClientNav from 'app/ClientNav';
import Footer from 'components/build/Footer';
import Navbar from 'components/build/Navbar';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { parseJwt } from 'utils/parseJwt';

const ClientLayout = ({ children }) => {
  const router = useRouter();

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt.role.role !== 'client') {
      router.push('/');
    }
  }, []);
  return (
    <>
      <Navbar logged={true} active={'Beranda'} />
      <div className='py-16 pt-32 min-h-screen'>
        <div className='flex justify-center gap-5'>
          <ClientNav />
          <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6 h-min'>
            {children}
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default ClientLayout;
