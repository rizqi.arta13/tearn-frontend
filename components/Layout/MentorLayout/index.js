import { LoadingOutlined } from '@ant-design/icons';
import MentorNav from 'app/MentorNav';
import Footer from 'components/build/Footer';
import Navbar from 'components/build/Navbar';
import { useRouter } from 'next/router';
import { createContext, useEffect, useState } from 'react';
import { parseJwt } from 'utils/parseJwt';

export const UserMentorContext = createContext();

const MentorLayout = ({ children }) => {
  const router = useRouter();
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    try {
      if (decodeJwt) {
        setUser(decodeJwt);
      }

      if (decodeJwt.role.role !== 'mentor') {
        throw Error('Unauthorized');
      } else {
        setLoading(false);
      }
    } catch (err) {
      router.push('/');
    }
  }, []);
  return (
    <>
      <UserMentorContext.Provider value={user}>
        {loading ? (
          <>
            <div className='w-full h-full flex items-center justify-center animate-pulse text-primary'>
              <LoadingOutlined
                style={{ fontSize: '48px' }}
                className='animate-spin text-primary'
              />
            </div>
          </>
        ) : (
          <>
            <Navbar logged={true} active={'Beranda'} />
            <div className='py-16 pt-32 k'>
              <div className='flex justify-center gap-5'>
                <MentorNav />
                {children}
              </div>
            </div>
            <Footer />
          </>
        )}
      </UserMentorContext.Provider>
    </>
  );
};

export default MentorLayout;
