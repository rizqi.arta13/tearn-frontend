import { LoadingOutlined } from '@ant-design/icons';

const Button = (props) => {
  const { className, onSubmit, type, children, loading, ...btnProps } = props;

  let typeColor;

  switch (type) {
    case 'disabled':
      typeColor = 'bg-gray-200 text-gray-700';
      break;
    case 'active':
      typeColor = 'bg-green-600 text-white';
      break;
    case 'danger':
      typeColor = 'bg-red-600 text-white';
      break;
    case 'warning':
      typeColor = 'text-white bg-[#dd9b19]';
      break;
    case 'primary':
    default:
      typeColor = 'bg-primary text-white';
      break;
  }
  return (
    <button
      className={className + ' h-[32px] w-[132px] rounded ' + typeColor}
      disabled={type === 'disabled' || loading ? true : false}
      onSubmit={onSubmit}
      {...btnProps}
    >
      <span className='flex justify-center items-center'>{children}</span>
    </button>
  );
};

export default Button;
