import { SwapRightOutlined } from '@ant-design/icons';
import { Avatar, Divider } from 'antd';
import React from 'react';

const GigsCard = (props) => {
  const { image, noImage, gigsTitle, rating, price, desc, mentor = [] } = props;

  return (
    <div className='flex flex-col gap-4'>
      <div className='flex flex-row w-full p-4 shadow-md rounded-xl border border-[#00000010] max-h-64 hover:shadow-lg transition-shadow'>
        <div className='flex w-7/12 gap-4'>
          <div className='flex items-center'>
            <Avatar size={92} src={image} className='bg-primaryTrans'>
              {noImage}
            </Avatar>
          </div>
          <div className='w-full flex flex-col justify-center'>
            <h6 className='font-medium m-0'>{mentor.join(' ')}</h6>
            <h3 className='leading-[24px] text-primary text-[18px] m-0'>{gigsTitle}</h3>
            <div className='m-0 flex mt-1'>
              <Avatar
                className='w-[16px] h-[16px] mr-1'
                src={'/img/icons/star.svg'}
                shape='square'
              />
              <h6 className='font-light m-0 text-[12px]'>{rating}</h6>
            </div>
          </div>
        </div>
        <div className='mr-4'>
          <Divider type='vertical' className='h-full bg-darkTrans' />
        </div>
        <div className='w-5/12 h-auto'>
          <div className='flex flex-col '>
            <h3 className='font-light'>
              <span className='font-semibold text-[18px]'>
                {new Intl.NumberFormat('id-ID', {
                  style: 'currency',
                  currency: 'IDR',
                }).format(price)}
              </span>{' '}
              /jam
            </h3>
            <p className='font-medium text-dark m-0'>Deskripsi</p>
            <div className='flex flex-col w-full'>
              <p className='max-h-[6ch] text-dark leading-[16px] text-ellipsis break-words m-0 truncate'>
                {desc}
              </p>
              <p>
                Lihat Selengkapnya <SwapRightOutlined />
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default GigsCard;
