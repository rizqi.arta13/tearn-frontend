const Select = (props) => {
    const { form, id, name, children, ...selectProps} = props

    return (
        <div>
            <select
                form={form}  
                id={id}
                name={name}
                className="px-3 py-[6px] w-full h-[32px] bg-light text-black font-[14px] border-darkTrans border-[1px] rounded placeholder:text-dark focus:outline-none"  
                {...selectProps}>
                {children}
            </select>
        </div>
    )
}

export default Select;