import Image from 'next/image';
import HeroImg from '../../public/grid-img.png';
import React from 'react';
import Link from 'next/link';

const Hero = () => {
  return (
    <>
      <div className='flex md:flex-row flex-col bg-gradient-to-br from-[#FFFFFF] to-[#D0D0D040] justify-between px-48 pt-4 gap-8'>
        <div className='w-1/2 h-[560px]'>
          <div className='flex flex-col items-center justify-center h-full'>
            <div className='flex flex-col'>
              <h1 className='text-[40px] font-bold'>
                Sensasi baru untuk <span className='text-primary'>belajar</span>{' '}
                dan <span className='text-primary'>mengajar</span>.
              </h1>
              <p className='font-regular text-[16px] text-text mb-[28px]'>
                Tingkatkan kualitas belajarmu dengan bantuan para mentor secara
                langsung!
              </p>
              <Link href={'/search-mentor' }>
                <span className='font-bold w-56 h-12 text-[18px] bg-primary text-light rounded flex items-center justify-center cursor-pointer'>
                  Cobain Sekarang!
                </span>
              </Link>
            </div>
          </div>
        </div>
        <div>
          <div className='flex flex-col items-center justify-center h-full'>
            <Image width={581} src={HeroImg} />
          </div>
        </div>
      </div>
    </>
  );
};

export default Hero;
