const Section = (props) => {
  const { children } = props;

  return (
    <section className='flex flex-col items-center md:mx-40 lg:mx-80 mb-48'>
      {children}
    </section>
  );
};

export default Section;
