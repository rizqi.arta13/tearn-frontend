import { useState, useEffect, useLayoutEffect } from 'react';
import Image from 'next/image';
import Logo from '../../public/logo.svg';
import { Avatar, Button, Divider, Dropdown, Menu, Space } from 'antd';
import Link from 'next/link';
import { CaretDownOutlined } from '@ant-design/icons';
import { parseJwt } from 'utils/parseJwt';

const Navbar = (props) => {
  const [user, setUser] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt) {
      setUser(decodeJwt);
    } else {
      setUser(false);
    }
  }, []);

  const link = [
    {
      href: '/',
      name: 'Beranda',
    },
    {
      href: '/search-mentor',
      name: 'Cari Mentor',
    },
    {
      href: '/about',
      name: 'Tentang Kami',
    },
  ];

  const menu = (
    <Menu
      items={[
        {
          label: <Link href='/logout'>Logout</Link>,
          key: '0',
        },
      ]}
    />
  );

  if (!user) {
    return (
      <>
        <div className='bg-light w-full px-[72px] py-[20px] z-50 flex flex-row shadow fixed'>
          <Link href={'/'} passHref>
            <a>
              <Image src={Logo} alt='logo' height={32} />
            </a>
          </Link>
          <div className='flex w-full flex-row gap-11 justify-center items-center'>
            {link.map((v, i) => {
              return (
                <Link href={v.href} key={i} passHref>
                  <div
                    className={`${
                      props.active === v.name ? 'text-dark ' : 'text-darkTrans'
                    } relative flex flex-col text-[14px] font-medium hover:text-dark transition-[0.5s] cursor-pointer `}
                  >
                    {v.name}
                  </div>
                </Link>
              );
            })}
          </div>
          <div className='flex gap-4 flex-row my-1'>
            <Link href={'/login'}>
              <a className='text-[14px] font-bold hover:text-dark transition-colors text-darkTrans cursor-pointer dark'>
                Masuk
              </a>
            </Link>
            <Divider type='vertical' className='h-full bg-darkTrans' />
            <Link href={'/register'}>
              <a className='text-[14px] font-bold hover:text-primaryTrans transition-colors text-primary cursor-pointer dark'>
                Daftar
              </a>
            </Link>
          </div>
        </div>
      </>
    );
  } else if (user.role.role === 'client') {
    return (
      <>
        <div className='bg-light w-full px-[72px] py-[20px] z-50 flex justify-between shadow fixed'>
          <Link href={'/'}>
            <a>
              <Image src={Logo} alt='logo' height={30} />
            </a>
          </Link>
          <div className='flex w-full flex-row gap-11 justify-center items-center'>
            {link.map((v, i) => {
              return (
                <Link href={v.href} key={i} passHref>
                  <a
                    className={`${
                      props.active === v.name ? 'text-dark ' : 'text-darkTrans'
                    } relative flex flex-col text-[14px] font-medium hover:text-dark transition-[0.5s] cursor-pointer `}
                  >
                    {v.name}
                  </a>
                </Link>
              );
            })}
          </div>
          <div className='flex gap-4 flex-row my-1'>
            <div className='flex justify-between'>
              <Dropdown
                overlay={menu}
                trigger={['click']}
                placement='topRight'
                arrow
                className='px-3 py-0 flex items-center'
              >
                <a onClick={(e) => e.preventDefault()}>
                  <Space>
                    <div className='flex justify-between gap-2 items-center text-dark'>
                      <Avatar
                        size={24}
                        src={user.profilePhoto}
                        className='bg-orange-500'
                      >
                        <span className='text-sm'>
                          {user.profilePhoto
                            ? ''
                            : user.username.charAt(0).toUpperCase()}
                        </span>
                      </Avatar>
                      <span>{user.username}</span>
                      <CaretDownOutlined className='text-primary text-[24px]' />
                    </div>
                  </Space>
                </a>
              </Dropdown>
            </div>
          </div>
        </div>
      </>
    );
  } else {
    return (
      <>
        <div className='bg-light w-full px-[72px] py-[20px] z-50 flex justify-between shadow fixed'>
          <Link href={'/'}>
            <a>
              <Image src={Logo} alt='logo' height={30} />
            </a>
          </Link>

          <div className='flex gap-4 flex-row my-1'>
            <div className='flex justify-between'>
              <Dropdown
                overlay={menu}
                trigger={['click']}
                placement='topRight'
                arrow
                className='px-3 py-0 flex items-center'
              >
                <a onClick={(e) => e.preventDefault()}>
                  <Space>
                    <div className='flex justify-between gap-2 items-center text-dark'>
                      <Avatar
                        size={24}
                        src={user.profilePhoto}
                        className='bg-orange-500'
                      >
                        <span className='text-sm'>
                          {user.profilePhoto
                            ? ''
                            : user.username.charAt(0).toUpperCase()}
                        </span>
                      </Avatar>
                      <span>{user.username}</span>
                      <CaretDownOutlined className='text-primary text-[24px]' />
                    </div>
                  </Space>
                </a>
              </Dropdown>
            </div>
          </div>
        </div>
      </>
    );
  }
};

export default Navbar;
