import { Divider, Image as AntImage } from 'antd';
import Image from 'next/image';
import Link from 'next/link';
import { Phone } from 'akar-icons';
import { InstagramOutlined, MailFilled, MailOutlined } from '@ant-design/icons';

const Footer = () => {
  return (
    <div className='w-full'>
      <div className='flex items-center justify-center bg-footer py-12 h-[240px]'>
        <div className='flex items-center justify-center'>
          <Image
            className='px-24 py-8'
            src={'/logo.svg'}
            width={480}
            height={72}
            alt='logo'
          />
        </div>
        <Divider type='vertical' className='h-full bg-dark' />
        <div className='flex flex-row px-24 gap-12'>
          <Subject />
          <EduLevel />
          <Contact />
        </div>
      </div>
      <div className='bg-[#00000090] flex flex-row justify-center text-light font-medium font-base  w-full p-4'>
        © TEARN - 2022
      </div>
    </div>
  );
};

const Subject = () => {
  return (
    <>
      <ul className='list-none flex flex-col m-0 text-left text-dark'>
        <h4 className='text-primary font-medium'>MATERI PELAJARAN</h4>
        <div className='r'>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Matematika</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Bahasa Indonesia</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Bahasa Inggris</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Sejarah</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Kimia</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Fisika</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Biologi</a>
            </Link>
          </li>
        </div>
      </ul>
    </>
  );
};

const EduLevel = () => {
  return (
    <>
      <ul className='list-none flex flex-col m-0 text-left text-dark'>
        <h4 className='text-primary font-medium'>TINGKAT PENDIDIKAN</h4>
        <div className=''>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>SD</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>SMP</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>SMA/K</a>
            </Link>
          </li>
          <li>
            <Link href={'/#'}>
              <a className='text-dark'>Umum</a>
            </Link>
          </li>
        </div>
      </ul>
    </>
  );
};

const Contact = () => {
  return (
    <>
      <ul className='list-none flex flex-col m-0 text-left text-dark'>
        <h4 className='text-primary font-medium'>KONTAK</h4>
        <div className=''>
          <li className='flex flex-row items-center'>
            <span>
              <Phone strokeWidth={2} size={12} />
            </span>
            <Link href={'/#'}>
              <a className='text-dark px-2'>(021) 40000640</a>
            </Link>
          </li>
          <li className='flex flex-row items-center'>
            <span>
              <MailOutlined strokeWidth={2} size={12} />
            </span>
            <Link href={'/#'}>
              <a className='text-dark px-2'>teachnlearn@gmail.com</a>
            </Link>
          </li>
          <li className='flex flex-row items-center'>
            <span>
              <InstagramOutlined strokeWidth={2} size={12} />
            </span>
            <Link href={'/#'}>
              <a className='text-dark px-2'>rizqiarta_fatullah</a>
            </Link>
          </li>
          <li className='flex flex-row items-center'>
            <span>
              <InstagramOutlined strokeWidth={2} size={12} />
            </span>
            <Link href={'/#'}>
              <a className='text-dark px-2'>bodybody03</a>
            </Link>
          </li>
        </div>
      </ul>
    </>
  );
};

export default Footer;
