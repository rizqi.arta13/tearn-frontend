const Input = (props) => {
  return (
    <div>
      <input
        type={props.type}
        id={props.id}
        name={props.name}
        placeholder={props.placeholder}
        value={props.value}
        className='px-3 py-2 w-[256px] h-[32px] bg-primaryLight text-black border-solid border-2 rounded placeholder:text-placeholder focus:outline-none'
      />
    </div>
  );
};

export default Input;
