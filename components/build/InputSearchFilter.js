const InputSearchFilter = (props) => {
  const { type, id, name, placeholder, value, className, onChange } = props;
  return (
    <div>
      <input
        type={type}
        id={id}
        name={name}
        placeholder={placeholder}
        value={value}
        className={
          className +
          ' pr-3 py-2 w-full h-[32px] bg-light text-black font-[14px] border-darkTrans border-[1px] rounded focus:outline-none'
        }
        onChange={onChange}
      />
    </div>
  );
};

export default InputSearchFilter;
