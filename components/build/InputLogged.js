const InputLogged = (props) => {
  const { autoComplete, type, id, name, placeholder, value, className } = props;
  return (
    <div>
      <input
        autoComplete={'true'}
        type={type}
        id={id}
        name={name}
        placeholder={placeholder}
        value={value}
        className={
          'px-3 py-[6px] w-full bg-light text-black font-[14px] border-darkTrans border-[1px] rounded placeholder:text-dark focus:outline-none' +
          className
        }
      ></input>
    </div>
  );
};

export default InputLogged;
