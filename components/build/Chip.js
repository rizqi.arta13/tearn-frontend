const Chip = (props) => {
  const { type, children } = props;

  let typeColor;

  switch (type) {
    case 'disabled':
      typeColor = 'bg-[#d8d8d8] text-[#878787]';
      break;
    case 'active':
      typeColor = 'bg-[#d2e6c8] text-[#4a9b22]';
      break;
    case 'danger':
      typeColor = 'bg-[#f2ccbf] text-[#cc3300]';
      break;
    case 'warning':
      typeColor = 'bg-[#f6e6c5] text-[#dd9b19]';
      break;
    case 'primary':
      typeColor = 'bg-[#c7d5e7] text-[#1e56a0]';
      break;
    default:
      typeColor = 'bg-gray-500 text-gray-700';
      break;
  }

  return (
    <div className={`rounded-full py-1 px-2 font-semibold w-fit ${typeColor}`}>
      {children}
    </div>
  );
};

export default Chip;
