import axios from 'axios';
import { useEffect, useState } from 'react';

const useAxios = (config) => {
  const [response, setResponse] = useState(null);
  const [error, setError] = useState(null);
  axios.defaults.baseURL = process.env.API_URL || 'http://localhost:3222/';

  const fetchData = async () => {
    const { method, url, data } = config;
    axios({ method, url, data })
      .then((res) => {
        setResponse(res.data);
      })
      .catch((err) => {
        setError(err);
      });
  };

  // Fetch
  useEffect(() => {
    fetchData();
  }, []);

  return { response, error };
};

export default useAxios;
