# How to use custom hooks

these hooks are built by for needs of this application

## useAxios({ url: string, method: string, data: Array | Object })

useAxios is hook used for fetching data using axios library

**example use case**

```javascript
import React, { useEffect, useState } from 'react';
import useAxios from 'hooks/useAxios';

const Test = () => {
  const [data, setData] = useState(null);
  // useAxios return an object with property response and error

  //you can set the default baseUrl with env using API_URL the default is http://localhost:3222/
  const { response, error } = useAxios({
    url: '/users?role=f646ad6e-b81d-4d39-9f86-6d7ee76cd681a',
    method: 'get',
  });


  // used for initial load
  useEffect(() => {
    if (response !== null) {
      setData(response.data);
    }
  }, [response]);

  return (
    <>
      {data.map((v, i) => {
        return <div>{v.fullname}</div>
      })}
    </div>
  );
};

export default Test;
```
