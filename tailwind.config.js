const withMT = require('@material-tailwind/react/utils/withMT');

module.exports = withMT({
  mode: 'jit',
  purge: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
    './app/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: '#1E56A0',
        primaryTrans: '#1E56A099',
        light: '#FFFFFF',
        dark: '#000000',
        darkTrans: '#00000050',
        primaryLight: '#EAF1F7',
        bodyBackground: '#F0F0F0',
        placeholder: '#808080',
        footer: '#D9D9D9',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  important: '#tailwind-selector',
});
