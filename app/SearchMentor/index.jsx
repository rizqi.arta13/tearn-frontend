import { Search } from 'akar-icons';
import { Select, Divider, Form, DatePicker, Space, Button } from 'antd';
import { useEffect, useState } from 'react';
import { gigRepository } from 'repository/gigs';
import style from './search-mentor.module.css';

const SearchMentor = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const { RangePicker } = DatePicker;

  const { Option } = Select;

  const { data: fetchData } = gigRepository.hooks.useSubject();

  useEffect(() => {
    if (fetchData) {
      setData(fetchData.data);
      setLoading(false);
    }
  }, [fetchData]);

  return (
    <>
      <div className='flex flex-col '>
        <div className='p-4 flex flex-col items-center '>
          <h1 className='font-semibold text-[24px]'>Cari Mentor</h1>
          <div className='w-1/2 flex flex-col items-center'>
            <p className='text-center text-[14px]'>
              Belajar privat secara tatap muka atau daring.
              <br />
              Tentukan apa yang kamu inginkan. Pilihan ada padamu.
            </p>
          </div>
        </div>
        <Form onFinish={(values) => props.onFinish(values)}>
          <div className='flex justify-between gap-6'>
            <div className='flex  w-[90%] py-4 px-4 bg-light rounded-2xl shadow-lg'>
              <div className='flex flex-col w-1/3'>
                <h3 className='text-[14px] px-[11px] font-medium m-0'>
                  Mata Pelajaran
                </h3>
                <div className='w-full  mt-[4px]'>
                  <Form.Item name='subject' className='m-0'>
                    <Select
                      placeholder='Pilih Mata Pelajaran'
                      // showArrow={false}
                      bordered={false}
                      className={style.width__select}
                    >
                      {data.map((v) => {
                        return (
                          <Option key={v.id} value={v.subject}>
                            {v.subject}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>
                </div>
              </div>
              <Divider type='vertical' className='h-full bg-darkTrans' />
              <div className='flex flex-col w-1/3'>
                <h3 className='text-[14px] px-[11px] font-medium m-0'>
                  Durasi Hari
                </h3>
                <div className='w-full  mt-[4px] flex items-center'>
                  <Space
                    direction='vertical'
                    size={12}
                    className='cursor-pointer'
                  >
                    <Form.Item name='scheduleDate' className='m-0'>
                      <RangePicker
                        bordered={false}
                        format='DD-MM-YYYY'
                        placeholder={['Tanggal Mulai', 'Tanggal Selesai']}
                      />
                    </Form.Item>
                  </Space>
                </div>
              </div>
              <Divider type='vertical' className='h-full bg-darkTrans' />
              <div className='flex flex-col w-1/3'>
                <h3 className='text-[14px] px-[11px] font-medium m-0'>
                  Durasi Jam
                </h3>
                <div className='w-full  mt-[4px]'>
                  <div className='flex gap-2 items-center'>
                    <Form.Item name='scheduleTime' className='m-0'>
                      <RangePicker
                        bordered={false}
                        format={'HH:mm'}
                        picker='time'
                        placeholder={['Jam Mulai', 'Jam Selesai']}
                      />
                    </Form.Item>
                  </div>
                </div>
              </div>
            </div>
            <div className='w-[10%] bg-primary rounded-2xl flex justify-center items-center shadow-lg hover:shadow-xl transition-shadow'>
              <Form.Item className='m-0 bg-transparent'>
                <Button
                  htmlType='submit'
                  className='bg-transparent p-4 h-full  border-0 outline-none'
                >
                  <Search className='text-light' size={52} />
                </Button>
              </Form.Item>
            </div>
          </div>
        </Form>
      </div>
    </>
  );
};

export default SearchMentor;

// ant-select-selector
