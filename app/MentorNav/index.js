import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { parseJwt } from 'utils/parseJwt';

const MentorNav = () => {
  const router = useRouter();
  const [user, setUser] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt) {
      setUser(decodeJwt);
    } else {
      setUser(false);
    }
  }, []);
  const nav = [
    {
      navigation: [
        {
          title: 'Profil pengguna',
          route: '/mentor/profile',
        },
      ],
    },
    ,
    {
      head: 'Pembelajaran',
      navigation: [
        {
          title: 'Daftar Pemesanan',
          route: '/mentor',
        },
        {
          title: 'Paket Pembelajaran',
          route: '/mentor/gigs',
        },
      ],
    },
    {
      head: 'Pembayaran',
      navigation: [
        {
          title: 'Daftar Pembayaran',
          route: '/mentor/payment',
        },
        {
          title: 'Riwayat Pembayaran',
          route: '/mentor/payment-history',
        },
      ],
    },
  ];
  return (
    <div className='h-min sticky top-24 max-w-xs px-8 py-4 pb-8 flex flex-col gap-8 shadow-md rounded bg-white'>
      <div>
        <h6 className='font-semibold text-2xl mb-3'>Dasbor</h6>
        <div className='text-sm mb-8'>
          Selamat datang, <span className='font-medium'>{user.username}</span>!
        </div>
      </div>
      <nav>
        <ul className='gap-2 pb-8'>
          {nav.map((links, i) => {
            return (
              <div key={i} className='flex flex-col gap-2'>
                {links.head ? (
                  <>
                    <div className='m-0 mt-2 w-full h-[1px] bg-gradient-to-r from-[#00000050] to-transparent'></div>
                    <h3 className='m-0 text-[14px]'>{links.head}</h3>
                  </>
                ) : (
                  ''
                )}
                {links.navigation.map((v, i) => {
                  return (
                    <li key={i} className='font-medium'>
                      <Link href={v.route} passHref>
                        <a
                          className={
                            router.pathname == v.route
                              ? 'font-semibold text-primary'
                              : 'text-darkTrans hover:text-primary'
                          }
                        >
                          {v.title}
                        </a>
                      </Link>
                    </li>
                  );
                })}
              </div>
            );
          })}
        </ul>
      </nav>
    </div>
  );
};

export default MentorNav;
