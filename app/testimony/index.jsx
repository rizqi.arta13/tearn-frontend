import { Avatar, Image } from "antd";
import "react-multi-carousel/lib/styles.css";
import Section from "components/build/Section";

const data = [
  {
    avatar: "/img/avatar/man0.jpeg",
    title: "agoes salimoes",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non volutpat ut ullamcorper facilisis donec nisi nec. Nisl augue.",
  },
  {
    avatar: "/img/avatar/woman0.jpeg",
    title: "khoirun",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non volutpat ut ullamcorper facilisis donec nisi nec. Nisl augue.",
  },
  {
    avatar: "/img/avatar/man1.jpeg",
    title: "salimoes agoes",
    desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non volutpat ut ullamcorper facilisis donec nisi nec. Nisl augue.",
  },
  // {
  //   avatar: '/img/avatar/woman1.jpeg',
  //   title: 'muhammad novi',
  //   desc: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non volutpat ut ullamcorper facilisis donec nisi nec. Nisl augue.',
  // },
];

const carouselResponsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 2560 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 2560, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};
const Testimony = () => {
  return (
    <Section>
      <h3 className="text-5xl inline-flex items-center text-center mb-16">
        <strong>
          Kata Mereka yang Udah Join <span className="text-primary">Tearn</span>
        </strong>
      </h3>
      <div>
        <div className="flex flex-row gap-8">
          {data.map((v, i) => {
            return (
              <div key={i}>
                <Card avatar={v.avatar} title={v.title} desc={v.desc} />
              </div>
            );
          })}
        </div>
      </div>
    </Section>
  );
};

const Card = (props) => {
  const { avatar, title, desc } = props;

  return (
    <div className="rounded-2xl shadow-md bg-white flex flex-col max-w-xs px-7 py-9 items-center">
      <Avatar src={avatar} size={136} alt="avatar" />
      <div className="text-lg font-bold py-6">{title}</div>
      <div className="text-base text-center">{desc}</div>
    </div>
  );
};

export default Testimony;
