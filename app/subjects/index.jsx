import { Image } from 'antd';
import Section from 'components/build/Section';

const data = [
  {
    icon: '/img/icons/language.svg',
    title: 'language',
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident, distinctio, eveniet vero repellat accusantium et optio tenetur doloribus dolorem cumque quis ad quam? Ab.',
  },
  {
    icon: '/img/icons/math.svg',
    title: 'math',
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident, distinctio, eveniet vero repellat accusantium et optio tenetur doloribus dolorem cumque quis ad quam? Ab.',
  },
  {
    icon: '/img/icons/history.svg',
    title: 'history',
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident, distinctio, eveniet vero repellat accusantium et optio tenetur doloribus dolorem cumque quis ad quam? Ab.',
  },
  {
    icon: '/img/icons/database.svg',
    title: 'database',
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident, distinctio, eveniet vero repellat accusantium et optio tenetur doloribus dolorem cumque quis ad quam? Ab.',
  },
  {
    icon: '/img/icons/terminal.svg',
    title: 'terminal',
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident, distinctio, eveniet vero repellat accusantium et optio tenetur doloribus dolorem cumque quis ad quam? Ab.',
  },
  {
    icon: '/img/icons/and-more.svg',
    title: 'and more',
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Provident, distinctio, eveniet vero repellat accusantium et optio tenetur doloribus dolorem cumque quis ad quam? Ab.',
  },
];

const Subjects = () => {
  return (
    <Section>
      <h3 className='text-5xl mb-16 text-center'>
        <strong>Mapel apa aja sih yang ada disini?</strong>
      </h3>

      {data.map((v, i) => {
        return (
          <div key={i} className='flex gap-5 mb-8 items-center'>
            <div className='rounded-full shadow-md p-4 bg-light flex'>
              <Image
                src={v.icon}
                alt={v.title}
                width={40}
                height={40}
                preview={false}
              />
            </div>
            <div className='text-base'>
              {v.desc}
            </div>
          </div>
        );
      })}
    </Section>
  );
};

export default Subjects;
