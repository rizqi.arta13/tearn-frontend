import { AutoComplete, Pagination } from 'antd';
import { useEffect, useState } from 'react';

const AdminTable = (props) => {
  const {
    columns = [],
    dataSource = [],
    title,
    onChangePagination,
    currentPage,
  } = props;
  const [current, setCurrent] = useState(currentPage);
  const [domLoaded, setDomLoaded] = useState(false);

  useEffect(() => {
    setDomLoaded(true);
  }, []);

  const handlePagination = async (page) => {
    setCurrent(page);
    onChangePagination(page);
  };

  return domLoaded ? (
    <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
      <header className='flex justify-between mb-3 '>
        <h6 className='ffont-medium text-[18px]'>{title}</h6>
        <AutoComplete className='w-64' placeholder='cari pengguna' />
      </header>
      <div>
        <table className='w-full text-left mb-9'>
          <thead>
            <tr className='bg-primaryLight h-10 border-b border-b-[rgba(0 0 0 0.5)] table-row'>
              {columns.map((col) => {
                return (
                  <th key={col.id} className='px-5 py-3 table-cell'>
                    {col.title}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {dataSource.map((data) => {
              return (
                <tr
                  key={data.id}
                  className='pb-3 h-10 border-b border-b-[rgba(0 0 0 0.5)]'
                >
                  {columns.map((col) => {
                    return (
                      <td key={col.id} className='px-5 py-3'>
                        {data[col.name]}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className='flex justify-center'>
          <Pagination
            current={current}
            onChange={handlePagination}
            total={50}
          />
        </div>
      </div>
    </div>
  ) : (
    <div>Loading</div>
  );
};

export default AdminTable;
