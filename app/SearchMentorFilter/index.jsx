import { Form, Button, Select, Slider } from 'antd';
import { useState } from 'react';

//price written in IDR
const MIN_PRICE = 10000;
const MAX_PRICE = 2500000;

const MIN_AGE = 18;
const MAX_AGE = 100;

const { Option } = Select;

const options = [
  {
    label: 'Matematika',
    value: 'matematika',
  },
  {
    label: 'Sejarah',
    value: 'sejarah',
  },
  {
    label: 'Bahasa Indonesia',
    value: 'bahasa indonesia',
  },
  {
    label: 'Fisika',
    value: 'fisika',
  },
  {
    label: 'Biologi',
    value: 'biologi',
  },
  {
    label: 'Bahasa Inggris',
    value: 'bahasa inggris',
  },
  {
    label: 'PHP',
    value: 'php',
  },
];

const currencyFormat = (value) => {
  return Intl.NumberFormat('id-ID', {
    style: 'currency',
    currency: 'IDR',
  }).format(value);
};

const SearchMentorFilter = (props) => {
  const [price, setPrice] = useState([MIN_PRICE, MAX_PRICE]);
  const [age, setAge] = useState([MIN_AGE, MAX_AGE]);

  const handleChange = (value) => {
    console.log(`selected ${value}`);
  };

  const handleFilterPrice = (value) => {
    setPrice([...value]);
  };

  const handleFilterAge = (value) => {
    setAge([...value]);
  };

  return (
    <div className='flex flex-col p-6 gap-6'>
      <b>Filter</b>
      <Form
        onFinish={(values) => props.onFinish(values)}
        initialValues={{
          age: [MIN_AGE, MAX_AGE],
          price: [MIN_PRICE, MAX_PRICE],
          educationLevel: 'TK',
        }}
        className='flex flex-col gap-6'
      >
        <div>
          <p className='text-base m-0'>Umur</p>
          <Form.Item name='age' className='m-0'>
            <Slider
              range
              trackStyle={{ backgroundColor: '#1E56A0' }}
              handleStyle={{ borderColor: '#1E56A0' }}
              min={MIN_AGE}
              max={MAX_AGE}
              value={age}
              onChange={handleFilterAge}
            />
          </Form.Item>
          <div>
            <b>{age[0]}</b> tahun - <b>{age[1]}</b> tahun
          </div>
        </div>
        <div>
          <p className='text-base m-0'>Harga Per Jam</p>
          <Form.Item name='price' className='m-0'>
            <Slider
              trackStyle={{ backgroundColor: '#1E56A0' }}
              handleStyle={{ borderColor: '#1E56A0' }}
              className='primary'
              range
              tipFormatter={currencyFormat}
              min={MIN_PRICE}
              max={MAX_PRICE}
              defaultValue={[MIN_PRICE, MAX_PRICE]}
              onChange={handleFilterPrice}
            />
          </Form.Item>
          <div>
            <b>{currencyFormat(price[0])}</b>/jam -{' '}
            <b className='overflow-visible'>
              {price[1] === MAX_PRICE ? 'seterusnya' : currencyFormat(price[1])}
            </b>
          </div>
        </div>
        <div>
          <p className='text-base mb-2'>Tingkat Pendidikan</p>
          <Form.Item name='educationLevel'>
            <Select
              size='medium'
              className='w-full'
              placeholder='Pilih Tingkatan'
              onChange={handleChange}
            >
              <Option value='TK'>TK</Option>
              <Option value='SD'>SD</Option>
              <Option value='SMP'>SMP</Option>
              <Option value='SMA/K'>SMA/K</Option>
              <Option value='UMUM'>UMUM</Option>
            </Select>
          </Form.Item>
        </div>
        <div>
          <Form.Item>
            <Button htmlType='submit'>Terapkan filter</Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
};

export default SearchMentorFilter;
