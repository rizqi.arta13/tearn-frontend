import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { parseJwt } from 'utils/parseJwt';

const AdminNav = () => {
  const router = useRouter();

  const [user, setUser] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt) {
      setUser(decodeJwt);
    } else {
      setUser(false);
    }
  }, []);

  const nav = [
    {
      title: 'Akun Pengguna',
      route: '/admin',
    },
    {
      title: 'Pembayaran',
      route: '/admin/payment',
    },
    {
      title: 'Mata Pelajaran',
      route: '/admin/subjects',
    },
  ];

  return (
    <>
      <div className='max-w-xs px-9 py-4 flex flex-col shadow-md rounded bg-white max-h-min h-full'>
        <div>
          <h6 className='font-semibold text-2xl mb-3'>Dasbor admin</h6>
          <div className='text-sm mb-14 text-ellipsis'>
            Selamat datang, <span className='font-medium'>{user.username}</span>
            !
          </div>
          <nav>
            <ul className='gap-2 pb-8'>
              <div className='flex flex-col gap-2'>
                {nav.map((v, i) => {
                  return (
                    <li key={i} className='font-medium'>
                      <Link href={v.route} passHref>
                        <a
                          className={
                            router.pathname == v.route
                              ? 'font-semibold text-primary'
                              : 'text-darkTrans hover:text-primary'
                          }
                        >
                          {v.title}
                        </a>
                      </Link>
                    </li>
                  );
                })}
              </div>
            </ul>
          </nav>
        </div>
      </div>
    </>
  );
};

export default AdminNav;
