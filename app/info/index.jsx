import { Avatar, Image } from 'antd';
import Section from 'components/build/Section';

const Info = () => {
  const data = [
    {
      icons: '/img/icons/login.png',
      text: 'Kamu bisa daftar jadi mentor juga lhoo!',
    },
    {
      icons: '/img/icons/file-search.svg',
      text: 'Pilih matapelajaran dan mentor yang kamu mau!',
    },
    {
      icons: '/img/icons/transaction.svg',
      text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    },
  ];
  return (
    <>
      <Section>
        <h3 className='text-5xl inline-flex items-center text-center my-16'>
          <strong>
            Kenalin Nih! <span className='text-primary'>Tearn</span>
          </strong>
        </h3>
        <div>
          <div className='flex flex-row gap-8'>
            {data.map((v, i) => {
              return (
                <div key={i}>
                  <Card icons={v.icons} text={v.text} />
                </div>
              );
            })}
          </div>
        </div>
      </Section>
    </>
  );
};

const Card = (props) => {
  const { icons, text } = props;
  return (
    <>
      <div
        className={
          'rounded-2xl shadow-md bg-white flex flex-col max-w-xs items-center'
        }
      >
        <div className='px-8 py-4 flex flex-col items-center justify-center'>
          <div className='min-h-[160px] mt-12 flex flex-row items-center justify-center'>
            <Avatar size={156} shape='square' src={icons} alt='image' />
          </div>
          <div className='min-h-[160px] flex flex-row items-center justify-center'>
            <div className='font-medium text-base text-center'>{text}</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Info;
