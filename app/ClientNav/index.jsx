import { ChevronRight } from 'akar-icons';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { parseJwt } from 'utils/parseJwt';

const ClientNav = () => {
  const router = useRouter();
  const [user, setUser] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt) {
      setUser(decodeJwt);
    } else {
      setUser(false);
    }
  }, []);

  const nav = [
    {
      navigation: [
        {
          title: 'Profil Pengguna',
          route: '/client/profile',
        },
      ],
    },
    ,
    {
      head: 'PEMBELAJARAN',
      navigation: [
        {
          title: 'Daftar Pembelajaran',
          route: '/client',
        },
        {
          title: 'Riwayat Pembelajaran',
          route: '/client/history',
        },
      ],
    },
    {
      head: 'PEMBAYARAN',
      navigation: [
        {
          title: 'Menunggu Pembayaran',
          route: '/client/payment',
        },
        {
          title: 'Riwayat Pembayaran',
          route: '/client/payment-history',
        },
      ],
    },
  ];

  return (
    <div className='h-min sticky top-24 max-w-xs px-8 py-4 pb-8 flex flex-col gap-8 shadow-md rounded bg-white'>
      <div>
        <h6 className='font-semibold text-2xl mb-3'>Dasbor</h6>
        <div className='text-sm mb-8'>
          Selamat datang, <span className='font-medium'>{user.username}</span>!
        </div>
      </div>
      <nav>
        <ul className='gap-2'>
          {nav.map((links, i) => {
            return (
              <div key={i} className='flex flex-col gap-2'>
                {links.head ? (
                  <>
                    <div className='m-0 mt-2 w-full h-[1px] bg-gradient-to-r from-[#00000050] to-transparent'></div>
                    <h3 className='m-0 text-[14px]'>{links.head}</h3>
                  </>
                ) : (
                  ''
                )}
                {links.navigation.map((v, i) => {
                  return (
                    <>
                      <li key={i} className='font-medium'>
                        <Link href={v.route} passHref>
                          <a
                            className={
                              router.pathname == v.route
                                ? 'font-semibold text-primary'
                                : 'text-darkTrans hover:text-primary'
                            }
                          >
                            {v.title}
                          </a>
                        </Link>
                      </li>
                    </>
                  );
                })}
              </div>
            );
          })}
        </ul>
      </nav>
      <div className='flex justify-self-end'>
        <div className='flex flex-col items-center gap-4 border border-primary rounded w-full py-4'>
          <DeviceIcon />
          <p className='text-center px-4 m-0'>
            Tertarik menjadi mentor?
            <br />
            Yuk gabung bersama kami!
          </p>
          <Link href='/register'>
            <a className='text-primary flex flex-row items-center'>
              <span>Daftar</span>
              <ChevronRight strokeWidth={2} size={12} />
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

const DeviceIcon = () => {
  return (
    <div>
      <svg
        width='55'
        height='55'
        viewBox='0 0 55 55'
        fill='none'
        xmlns='http://www.w3.org/2000/svg'
      >
        <rect width='55' height='55' rx='27.5' fill='#1E56A0' />
        <path
          d='M33.7 40H21.3M43 18.125C43 17.2962 42.6734 16.5013 42.092 15.9153C41.5107 15.3292 40.7222 15 39.9 15H15.1C14.2778 15 13.4893 15.3292 12.908 15.9153C12.3266 16.5013 12 17.2962 12 18.125V32.1875C12 33.0163 12.3266 33.8112 12.908 34.3972C13.4893 34.9833 14.2778 35.3125 15.1 35.3125H39.9C40.7222 35.3125 41.5107 34.9833 42.092 34.3972C42.6734 33.8112 43 33.0163 43 32.1875V18.125Z'
          stroke='white'
          strokeWidth='3'
          strokeLinecap='round'
          strokeLinejoin='round'
        />
      </svg>
    </div>
  );
};

export default ClientNav;
