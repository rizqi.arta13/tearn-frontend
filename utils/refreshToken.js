import jwtDecode from "jwt-decode";
import cookieCutter from "cookie-cutter";

export const refreshToken = () => {
  try {
    const decoded = jwtDecode(
      cookieCutter.get("token")
    );
    return decoded;
  } catch (e) {}
};
