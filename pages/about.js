import Navbar from 'components/build/Navbar';
import Footer from 'components/build/Footer';
import { Avatar, Button } from 'antd';

const Home = () => {
  return (
    <div className='flex flex-col items-center justify-center h-screen'>
      <p className='text-2xl font-medium mb-4'>Halaman Ini Belum Tersedia</p>
      <Avatar shape='square' src={'img/icons/not_found.svg'} size={400} />
      <a
        className='px-4 py-2 bg-light text-primary border-[2px] border-primary font-semibold rounded-2xl hover:shadow-xl transition-shadow'
        href='javascript:history.back()'
      >
        Kembali ke halaman sebelumnya
      </a>
    </div>
  );
};

export default Home;
