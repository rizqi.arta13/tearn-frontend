import { InstagramFill, WhatsappFill } from 'akar-icons';
import { Avatar, Divider, Rate } from 'antd';
import GigsCard from 'components/build/GigsCard';
import DetailMentorLayout from 'components/Layout/DetailMentorLayout';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { data } from './data';

const MentorDetail = () => {
  const router = useRouter();
  return (
    <>
      <div className='w-full py-8 px-48 flex justify-center'>
        <div className='flex flex-col p-8 bg-light shadow-lg rounded-md w-11/12 gap-8'>
          <h1 className='text-[18px]'>Detail Mentor</h1>
          <div className='flex gap-14'>
            <div className='flex flex-col gap-8'>
              <div className='flex flex-col items-center gap-4 rounded-md border-2 border-[#00000020] py-8'>
                <div className='flex flex-col items-center'>
                  <Avatar size={128} className='bg-primaryTrans'>
                    S
                  </Avatar>
                </div>
                <p className='leading-[32px] text-dark text-[28px] m-0'>
                  Jarjit Singh
                </p>
                <p className='m-0 text-xs text-[#00000080]'>
                  Pendidikan Terakhir
                </p>
              </div>
              <div className='flex flex-col mb-6 w-full'>
                <h1 className='text-[16px]'>Tentang Jarjit Sighn</h1>
                <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />
                <div className='flex flex-col pt-8 gap-2'>
                  <div className='font-medium'>Sertifikasi</div>
                  <ul className='w-full space-y-1 list-disc ml-6'>
                    <li>
                      2nd Winner Of The 2019 Coreldraw International Contest
                    </li>
                    <li>
                      1st Winner Of The 2020 Coreldraw International Contest
                    </li>
                    <li>Mentoring via daring</li>
                    <li>
                      Pembatalan pembelajaran dilakukan 2 jam sebelum jam
                      pelajaran dimulai
                    </li>
                  </ul>
                </div>
                <div className='flex flex-col pt-8 gap-2'>
                  <div className='font-medium'>
                    Mata Pelajaran yang dikuasai
                  </div>
                  <ul className='w-full flex gap-2'>
                    <li className='px-2 py-1 rounded-xl border-2'>
                      Javascript
                    </li>
                    <li className='px-2 py-1 rounded-xl border-2'>MySQL</li>
                  </ul>
                </div>
                <div className='flex flex-col pt-8 gap-2'>
                  <div className='font-medium'>Kontak</div>
                  <ul className='w-full'>
                    <li>
                      <Link href={'https://instagram.com/rizqiarta_fatullah'}>
                        <a className='flex items-center gap-2'>
                          <InstagramFill strokeWidth={2} size={14} />{' '}
                          rizqiarta_fatullah
                        </a>
                      </Link>
                    </li>
                    <li>
                      <Link href={'https://wa.me/62895332077465'}>
                        <a className='flex items-center gap-2'>
                          <WhatsappFill strokeWidth={2} size={14} />{' '}
                          080808080808
                        </a>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className='flex flex-col gap-16 w-8/12'>
              <div>
                <h1 className='text-[16px]'>Paket Lainnya dari Kon</h1>
                <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />
                <div className='flex flex-col gap-6 pt-6'>
                  {data.map((v, i) => {
                    return (
                      <>
                        <Link
                          key={i}
                          href={router.pathname + '?id=' + v.id}
                          passHref
                        >
                          <a>
                            <GigsCard
                              image={v.mentor.user.profilePhoto}
                              noImage={
                                v.mentor.user.profilePhoto
                                  ? ''
                                  : v.mentor.user.firstName
                                      .charAt(0)
                                      .toUpperCase()
                              }
                              gigsTitle={v.title}
                              rating={v.rate}
                              price={v.harga}
                              desc={v.desc}
                              mentor={[
                                v.mentor.user.firstName,
                                v.mentor.user.lastName,
                              ]}
                            />
                          </a>
                        </Link>
                      </>
                    );
                  })}
                </div>
              </div>

              <div className='flex flex-col w-full'>
                <h1 className='text-[16px] m-0'>Penilaian</h1>
                <div className='flex items-center gap-4'>
                  <Rate disabled allowHalf defaultValue={4.5} />
                  <p className='m-0'>
                    <span className='font-semibold'>4.7 </span>(dari 20 ulasan)
                  </p>
                </div>
                <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />

                <div className='w-full flex pt-6'>
                  <div className='w-5/12 flex gap-4'>
                    <div className='flex items-center'>
                      <Avatar size={40} className='bg-primaryTrans'>
                        A
                      </Avatar>
                    </div>
                    <div className='w-full flex flex-col justify-center'>
                      <p className='font-medium m-0 mb-1 text-[12px] leading-[12px]'>
                        Alexis Messi
                      </p>
                      <p className='m-0 text-[12px] leading-[12px]'>
                        12 November 2022
                      </p>
                    </div>
                  </div>
                  <div className='w-full flex flex-col'>
                    <Rate
                      disabled
                      allowHalf
                      defaultValue={4.5}
                      className='text-sm'
                    />
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Pulvinar et dui, cursus amet, orci, phasellus tincidunt.
                      Ullamcorper faucibus vestibulum, vitae in pretium diam non
                      egestas. Viverra lectus non mi vel. Massa hendrerit
                      gravida sed est elit quis arcu magna felis.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MentorDetail;

MentorDetail.getLayout = function Layout(page) {
  return <DetailMentorLayout>{page}</DetailMentorLayout>;
};
