export const data = [
  {
    id: 1,
    gigs: {
      id: 1,
      title: 'Paket UN mantap',
      subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
      price: 500000,
      rate: 4.7,
    },
    user: {
      username: 'bodybody',
    },
    status: 'Selesai',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
    method: 'Non-Tunai',
    proofImage:
      'http://assets.kompasiana.com/items/album/2019/05/15/20190515-042338-5cdb370875065776065e29e6.jpg',
  },
  {
    id: 2,
    gigs: {
      id: 1,
      title: 'Paket UN mantap',
      subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
      price: 50000,
      rate: 4.2,
    },
    user: {
      username: 'arthentic',
      profile: 'https://thispersondoesnotexist.com/image',
    },
    status: 'Selesai',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
    method: 'Tunai',
    proofImage:
      'https://awsimages.detik.net.id/community/media/visual/2016/03/08/0f009879-d609-4cce-8f40-be7d8a6a46ac_43.jpg?w=700&q=90',
  },
  {
    id: 3,
    gigs: {
      id: 2,
      title: 'Makin dekat dengan yesus',
      subjects: ['1001 alkitab', 'Sejarah nasrani'],
      price: 255000,
      rate: 4.6,
    },
    user: {
      username: 'GapingDragon07',
    },
    status: 'Selesai',
    startDate: '2022-08-13',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
    method: 'Non-Tunai',
    proofImage:
      'http://assets.kompasiana.com/items/album/2019/05/15/20190515-042338-5cdb370875065776065e29e6.jpg',
  },
];
