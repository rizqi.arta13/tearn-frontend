import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { data } from './data';
import { Avatar, Modal, Button as AntButton, Upload, Rate } from 'antd';
import Button from 'components/build/Button';
import Chip from 'components/build/Chip';
import ClientLayout from 'components/Layout/ClientLayout';
import { useEffect, useState } from 'react';
import { format } from 'date-fns';
import style from './payment.module.css';
import { Warning } from 'postcss';

const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
};

const ClientPaymentHsitory = (props) => {
  const [loading, setLoading] = useState(true);
  const [imgLoading, setImgLoading] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState([]);
  const [imageUrl, setImageUrl] = useState([]);

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
    setImageUrl([]);
  };

  const onBayar = (id) => {
    const data = {
      image: imageUrl[id],
    };
    // data dikirm

    handleOk(id);
  };

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);

  return loading ? (
    <div className='w-full h-full flex items-center justify-center animate-pulse text-primary'>
      <LoadingOutlined
        style={{ fontSize: '48px' }}
        className='animate-spin text-primary'
      />
    </div>
  ) : (
    <>
      <div className='bg-white w-full rounded'>
        <header className='flex justify-between m-0'>
          <h6 className='font-medium text-[18px]'>Menunggu Pembayaran</h6>
        </header>
        {data.map((v) => {
          let type;

          switch (v.status) {
            case 'berjalan':
              type = 'primary';
              break;
            case 'Selesai':
              type = 'active';
              break;
            case 'Menunggu':
              type = 'warning';
              break;
            case 'belum dibayar':
              type = 'danger';
              break;
            default:
              type = 'disabled';
              break;
          }

          const handleChange = (info, id) => {
            if (info.file.status === 'uploading') {
              setImgLoading(true);
              return;
            }

            let tempImgUrl = imageUrl.slice();
            tempImgUrl[id] = null;

            if (info.file.status === 'done') {
              // Get this url from response in real world.
              getBase64(info.file.originFileObj, (url) => {
                setImgLoading(false);
                tempImgUrl.forEach((v) => {
                  v = null;
                });
                tempImgUrl[id] = url;
                setImageUrl(tempImgUrl);
              });
            }
          };

          const UploadButton = (
            <div className='flex flex-col justify-center items-center'>
              {imgLoading ? <LoadingOutlined /> : <PlusOutlined />}
              <span
                style={{
                  display: 'block',
                  marginTop: 8,
                }}
              >
                Upload
              </span>
            </div>
          );

          return (
            <div key={v.id} className='mt-4 flex flex-col'>
              <div className='flex flex-row gap-6 w-full rounded-xl shadow-md p-4 border border-[#00000010]'>
                <div className='w-9/12 flex flex-row gap-6'>
                  <div className='w-2/12 flex items-center'>
                    <Avatar
                      size={92}
                      src={v.user.profile}
                      className='bg-primaryTrans'
                    >
                      {v.user.profile
                        ? ''
                        : v.user.username.charAt(0).toUpperCase()}
                    </Avatar>
                  </div>
                  <div className='w-10/12'>
                    <h3 className='text-primary text-[18px] m-0'>
                      {v.gigs.title}
                    </h3>
                    <h6 className='font-light m-0'>{v.user.username}</h6>
                    <div className='m-0 flex'>
                      <Avatar
                        className='w-[16px] h-[16px] mr-1'
                        src={'/img/icons/star.svg'}
                        shape='square'
                      />
                      <h6 className='font-light m-0 text-[12px]'>
                        {v.gigs.rate}
                      </h6>
                    </div>
                    <Chip type={type}>{v.status}</Chip>
                  </div>
                </div>
                <div className='w-3/12 flex justify-end'>
                  <div className='flex flex-col justify-between'>
                    <div className='flex justify-end'>
                      <div>
                        <h6 className='font-semibold text-[18px]'>
                          <span>
                            {new Intl.NumberFormat('id-ID', {
                              style: 'currency',
                              currency: 'IDR',
                            }).format(v.gigs.price)}
                          </span>{' '}
                          <span className='font-normal text-[12px]'>total</span>
                        </h6>
                      </div>
                    </div>
                    <div className='flex gap-4'>
                      <div className='flex justify-end'>
                        <Button
                          onClick={() => showModal(v.id)}
                          className='font-medium text-[16px]'
                        >
                          Lihat Detail
                        </Button>
                        <Modal
                          key={v.id}
                          visible={isModalVisible[v.id]}
                          onOk={() => handleOk(v.id)}
                          onCancel={() => handleCancel(v.id)}
                          className='p-9'
                          width={900}
                          footer={[
                            <AntButton
                              key='Block'
                              type='warning'
                              warn
                              onClick={() => handleCancel(v.id)}
                            >
                              Batal
                            </AntButton>,
                            <AntButton
                              key='back'
                              type='primary'
                              onClick={() => onBayar(v.id)}
                            >
                              Unduh
                            </AntButton>,
                          ]}
                        >
                          <div className=''>
                            <h3 className='font-medium text-[18px] mb-6'>
                              Upload Bukti Pembayaran
                            </h3>
                            <div className='flex w-full '>
                              <div className='flex flex-col gap-4 w-8/12'>
                                <div className='flex'>
                                  <div className='w-2/6 font-medium text-[14px]'>
                                    Nama Paket
                                  </div>
                                  <div className='w-4/6 text-[14px]'>
                                    {v.gigs.title}
                                  </div>
                                </div>
                                <div className='flex'>
                                  <div className='w-2/6 font-medium text-[14px]'>
                                    Mata Pelajaran
                                  </div>
                                  <div className='w-4/6 '>
                                    <ul className='text-[14px]'>
                                      {v.gigs.subjects.map((v, i) => {
                                        return (
                                          <li key={i}>
                                            {'- '}
                                            {v}
                                          </li>
                                        );
                                      })}
                                    </ul>
                                  </div>
                                </div>
                                <div className='flex'>
                                  <div className='w-2/6 font-medium text-[14px]'>
                                    Estimasi Waktu
                                  </div>
                                  <div className='w-4/6 text-[14px]'>
                                    <span className='text-primary'>
                                      {v.startDate}{' '}
                                    </span>
                                    {' - '}
                                    <span className='text-primary'>
                                      {v.endDate}
                                    </span>
                                  </div>
                                </div>
                                <div className='flex'>
                                  <div className='w-2/6 font-medium text-[14px]'>
                                    Estimasi Jam
                                  </div>
                                  <div className='w-4/6 text-[14px]'>
                                    <span className='text-primary'>
                                      {format(new Date(v.startHour), 'HH:mm')}{' '}
                                    </span>
                                    {' - '}
                                    <span className='text-primary'>
                                      {format(new Date(v.endHour), 'HH:mm')}
                                    </span>
                                  </div>
                                </div>
                                <div className='flex'>
                                  <div className='w-2/6 font-medium text-[14px]'>
                                    Status
                                  </div>
                                  <div className='w-4/6 text-[14px]'>
                                    <Chip type={type}>{v.status}</Chip>
                                  </div>
                                </div>
                                <div className='flex'>
                                  <div className='w-2/6 font-medium text-[14px]'>
                                    Metode Pembayaran
                                  </div>
                                  <div className='w-4/6 text-[14px]'>
                                    {v.method}
                                  </div>
                                </div>
                                <div className='flex justify-between'>
                                  <div className='w-2/6 font-medium text-[14px]'>
                                    Total Bayar
                                  </div>
                                  <div className='w-4/6 text-[14px]'>
                                    <div className='flex'>
                                      <span className='text-18 font-semibold text-primary'>
                                        {new Intl.NumberFormat('id-ID', {
                                          style: 'currency',
                                          currency: 'IDR',
                                        }).format(v.gigs.price)}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div className='flex flex-col w-4/12 gap-4'>
                                <div className='flex flex-col gap-2'>
                                  <div className='font-medium text-[14px]'>
                                    Bukti Pembayaran
                                  </div>
                                  <div className='w-full h-full'>
                                    <img
                                      src={v.proofImage}
                                      alt='avatar'
                                      style={{
                                        width: '100%',
                                        height: 'auto',
                                        overflow: 'hidden',
                                      }}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </Modal>
                      </div>
                      <div className='flex justify-end'>
                        <Button
                          onClick={() => showModal(v.id + 'ulasan')}
                          className='font-medium text-[16px]'
                          type='warning'
                        >
                          Beri ulasan
                        </Button>
                        <Modal
                          key={v.id + 'ulasan'}
                          visible={isModalVisible[v.id + 'ulasan']}
                          onOk={() => handleOk(v.id + 'ulasan')}
                          onCancel={() => handleCancel(v.id + 'ulasan')}
                          className='p-9'
                          width={600}
                          footer={[
                            <AntButton
                              key='Block'
                              type='warning'
                              warn
                              onClick={() => handleCancel(v.id + 'ulasan')}
                            >
                              Batal
                            </AntButton>,
                            <AntButton
                              key='back'
                              type='primary'
                              onClick={() => onBayar(v.id)}
                            >
                              Kirim Ulasan
                            </AntButton>,
                          ]}
                        >
                          <h3 className='font-medium text-[18px] pl-8 mb-6'>
                            Beri Ulasan Untuk Paket Ini
                          </h3>
                          <div className='flex flex-col items-center'>
                            <p className='text-2xl mb-2 text-primary font-semibold'>
                              {v.gigs.title}
                            </p>
                            <Avatar
                              size={80}
                              src={v.user.profile}
                              className='bg-primaryTrans mb-2'
                            >
                              {v.user.profile
                                ? ''
                                : v.user.username.charAt(0).toUpperCase()}
                            </Avatar>
                            <p className='text-sm font-light m-0'>
                              {v.user.username}
                            </p>
                            <Rate allowHalf className='text-3xl' />
                            <span className='px-8 py-2 w-full'>
                              <textarea
                                className='p-2 w-full h border '
                                placeholder='Beri ulasan'
                                maxLength={500}
                              ></textarea>
                            </span>
                          </div>
                        </Modal>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default ClientPaymentHsitory;

ClientPaymentHsitory.getLayout = function Layout(page) {
  return <ClientLayout>{page}</ClientLayout>;
};
