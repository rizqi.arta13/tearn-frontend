export const data = [
  {
    id: 1,
    mentor: "Inumaki",
    schedules: ["senin", "selasa", "kamis"],
    startDate: "2022-08-12",
    endDate: "2022-08-15",
    startHour: "2014-01-01 11:00:00.000 +0700",
    endHour: "2014-01-01 15:00:00.000 +0700",
    status: "Berjalan",
    gigs: {
      title: "Paket UN mantap",
      subjects: ["Bahasa Indonesia", "Bahasa Inggris", "Sejarah"],
      harga: 50000,
    },
  },
  {
    id: 2,
    mentor: "Inumaki",
    schedules: ["senin", "selasa", "kamis"],
    startDate: "2022-08-12",
    endDate: "2022-08-15",
    startHour: "2014-01-01 11:00:00.000 +0700",
    endHour: "2014-01-01 15:00:00.000 +0700",
    status: "Menunggu",
    gigs: {
      title: "Makin dekat dengan Allah",
      subjects: ["Sholat tutor", "Bisa Quran", "Sejarah"],
      harga: 50000,
    },
  },
  {
    id: 3,
    mentor: "Inumaki",
    schedules: ["senin", "selasa", "kamis"],
    startDate: "2022-08-12",
    endDate: "2022-08-15",
    startHour: "2014-01-01 11:00:00.000 +0700",
    endHour: "2014-01-01 15:00:00.000 +0700",
    status: "Selesai",
    gigs: {
      title: "Makin dekat dengan yesus",
      subjects: ["1001 alkitab", "Sejarah nasrani"],
      harga: 500000,
    },
  },
  {
    id: 4,
    mentor: "Inumaki",
    schedules: ["senin", "selasa", "kamis"],
    startDate: "2022-08-12",
    endDate: "2022-08-15",
    startHour: "2014-01-01 11:00:00.000 +0700",
    endHour: "2014-01-01 15:00:00.000 +0700",
    status: "Menunggu",
    gigs: {
      title: "Paket UN mantap",
      subjects: ["Bahasa Indonesia", "Bahasa Inggris", "Sejarah"],
      harga: 50000,
    },
  },
  {
    id: 5,
    mentor: "Inumaki",
    schedules: ["senin", "selasa", "kamis"],
    startDate: "2022-08-12",
    endDate: "2022-08-15",
    startHour: "2014-01-01 11:00:00.000 +0700",
    endHour: "2014-01-01 15:00:00.000 +0700",
    status: "Dibatalkan",
    gigs: {
      title: "Paket Dirasat maean",
      subjects: ["Bahasa Arab", "fiqh", "sejarah islami"],
      harga: 150000,
    },
  },
];
