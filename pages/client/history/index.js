import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import { data } from './data';
import {
  Avatar,
  Modal,
  Button as AntButton,
  Upload,
  Select,
  Pagination,
} from 'antd';
import Button from 'components/build/Button';
import Chip from 'components/build/Chip';
import ClientLayout from 'components/Layout/ClientLayout';
import { useEffect, useState } from 'react';
import { format } from 'date-fns';
import { transactionRepository } from 'repository/transaction';

const ClientOrderHistory = (props) => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [isModalVisible, setIsModalVisible] = useState([]);
  const [current, setCurrent] = useState(1);

  const [pageSize, setPageSize] = useState(10);
  const [search, setSearch] = useState('');
  const [count, setCount] = useState(0);

  const { data: fetchData } = transactionRepository.hooks.useOrderHistory({
    page: current,
    limit: pageSize,
  });

  useEffect(() => {
    if (fetchData !== null && fetchData !== undefined) {
      console.log(fetchData);
      setData(fetchData.result[0]);
      setCount(fetchData.result[1]);
      setTimeout(() => {
        setLoading(false);
      }, 500);
    }
  }, [fetchData]);

  const handlePagination = async (page) => {
    setCurrent(page);
  };

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  return loading ? (
    <div className='w-full min-h-[512px] flex items-center grow justify-center animate-pulse text-primary'>
      <LoadingOutlined
        style={{ fontSize: '48px' }}
        className='animate-spin text-primary'
      />
    </div>
  ) : (
    <>
      <div className='bg-white w-full rounded'>
        <header className='flex justify-between m-0'>
          <h6 className='font-medium text-[18px]'>Riwayat Pembelajaran</h6>
        </header>
        {data.map((v) => {
          let type;

          switch (v.status) {
            case 'berjalan':
              type = 'primary';
              break;
            case 'Selesai':
            case 'selesai':
              type = 'active';
              break;
            case 'Menunggu':
              type = 'warning';
              break;
            case 'belum dibayar':
              type = 'danger';
              break;
            default:
              type = 'disabled';
              break;
          }

          return (
            <div key={v.id} className='mt-4 flex flex-col mb-6'>
              <div className='flex flex-row gap-6 w-full rounded-xl shadow-md p-4 border border-[#00000010]'>
                <div className='w-9/12 flex flex-row gap-6'>
                  <div className='w-2/12 flex items-center'>
                    <Avatar
                      size={92}
                      src={v.gig.mentor.user.profilePhoto}
                      className='bg-primaryTrans'
                    >
                      {v.gig.mentor.user.profilePhoto
                        ? ''
                        : v.gig.mentor.user.username.charAt(0).toUpperCase()}
                    </Avatar>
                  </div>
                  <div className='w-10/12'>
                    <h3 className='text-primary text-[18px] m-0'>
                      {v.gig.title}
                    </h3>
                    <h6 className='font-light m-0'>
                      {v.gig.mentor.user.firstName} {v.gig.mentor.user.lastName}
                    </h6>
                    <div className='m-0 flex'>
                      <Avatar
                        className='w-[16px] h-[16px] mr-1'
                        src={'/img/icons/star.svg'}
                        shape='square'
                      />
                      <h6 className='font-light m-0 text-[12px]'>
                        {/* {v.gigs.rate} */} 4.6
                      </h6>
                    </div>
                    <Chip type={type}>{v.status}</Chip>
                  </div>
                </div>
                <div className='w-3/12 flex justify-end'>
                  <div className='flex flex-col justify-between'>
                    <div className='flex justify-end'>
                      <div>
                        <h6 className='font-semibold text-[18px]'>
                          <span>
                            {new Intl.NumberFormat('id-ID', {
                              style: 'currency',
                              currency: 'IDR',
                            }).format(v.amount)}
                          </span>{' '}
                          <span className='font-normal text-[12px]'>total</span>
                        </h6>
                      </div>
                    </div>
                    <div className='flex justify-end'>
                      <Button
                        onClick={() => showModal(v.id)}
                        className='font-medium text-[16px]'
                      >
                        Lihat Detail
                      </Button>
                      <Modal
                        key={v.id}
                        visible={isModalVisible[v.id]}
                        onOk={() => handleOk(v.id)}
                        onCancel={() => handleCancel(v.id)}
                        className='p-9'
                        footer={[
                          <AntButton
                            key='back'
                            type='primary'
                            warn
                            onClick={() => handleCancel(v.id)}
                          >
                            Kembali
                          </AntButton>,
                        ]}
                      >
                        <div className=''>
                          <h3 className='font-medium text-[18px] mb-6'>
                            Detail Pembelajaran
                          </h3>
                          <div className='flex w-full '>
                            <div className='flex flex-col gap-4 w-full'>
                              <div className='flex'>
                                <div className='w-full font-medium text-[14px]'>
                                  Nama Paket
                                </div>
                                <div className='w-full text-[14px]'>
                                  {v.gig.title}
                                </div>
                              </div>
                              <div className='flex'>
                                <div className='w-full font-medium text-[14px]'>
                                  Mata Pelajaran
                                </div>
                                <div className='w-full '>
                                  <ul className='text-[14px]'>
                                    {v.gig.subjects.map((v, i) => {
                                      return (
                                        <li key={i}>
                                          {'- '}
                                          {v.subjectId.subject}
                                        </li>
                                      );
                                    })}
                                  </ul>
                                </div>
                              </div>
                              <div className='flex'>
                                <div className='w-full font-medium text-[14px]'>
                                  Estimasi Waktu
                                </div>
                                <div className='w-full text-[14px]'>
                                  <span className='text-primary'>
                                    {v.startDate}{' '}
                                  </span>
                                  {' - '}
                                  <span className='text-primary'>
                                    {v.endDate}
                                  </span>
                                </div>
                              </div>
                              <div className='flex'>
                                <div className='w-full font-medium text-[14px]'>
                                  Estimasi Jam
                                </div>
                                <div className='w-full text-[14px]'>
                                  <span className='text-primary'>
                                    {format(new Date(v.startHour), 'HH:mm')}{' '}
                                  </span>
                                  {' - '}
                                  <span className='text-primary'>
                                    {format(new Date(v.endHour), 'HH:mm')}
                                  </span>
                                </div>
                              </div>
                              <div className='flex'>
                                <div className='w-full font-medium text-[14px]'>
                                  Status
                                </div>
                                <div className='w-full text-[14px]'>
                                  <Chip type={type}>{v.status}</Chip>
                                </div>
                              </div>
                              <div className='flex justify-between'>
                                <div className='w-full font-medium text-[14px]'>
                                  Total Bayar
                                </div>
                                <div className='w-full text-[14px]'>
                                  <div className='flex'>
                                    <span className='text-18 font-semibold text-primary'>
                                      {new Intl.NumberFormat('id-ID', {
                                        style: 'currency',
                                        currency: 'IDR',
                                      }).format(v.amount)}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </Modal>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
        <div className='flex justify-center'>
          <Pagination
            current={current}
            onChange={handlePagination}
            total={count}
          />
        </div>
      </div>
    </>
  );
};

export default ClientOrderHistory;

ClientOrderHistory.getLayout = function Layout(page) {
  return <ClientLayout>{page}</ClientLayout>;
};
