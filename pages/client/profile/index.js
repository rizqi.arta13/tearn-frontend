import { LoadingOutlined } from '@ant-design/icons';
import { location } from './location';
import {
  Avatar,
  Button,
  DatePicker,
  Form,
  message,
  Upload,
  Select as AntdSelect,
  Input,
} from 'antd';
import InputLogged from 'components/build/InputLogged';
import Select from 'components/build/Select';
import ClientLayout from 'components/Layout/ClientLayout';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { parseJwt } from 'utils/parseJwt';
import moment from 'moment';

const ClientProfile = (props) => {
  const router = useRouter();
  const [user, setUser] = useState(false);

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);

    if (decodeJwt) {
      setUser(decodeJwt);
    } else {
      setUser(false);
    }
  }, []);

  const [prov, setProv] = useState('');
  const [loading, setLoading] = useState(true);

  const handleSelectChange = (e) => {
    setProv(e);
  };

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);

  const propsUpload = {
    name: 'file',
    action: '',
    headers: {
      authorization: 'authorization-text',
    },

    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }

      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const dateFormat = 'DD/MM/YYYY';

  const handleGender = (value) => {
    console.log(`selected ${value}`);
  };

  const onChange = (e) => {
    console.log('Change:', e.target.value);
  };

  return loading ? (
    <div className='w-full h-full flex items-center justify-center animate-pulse text-primary'>
      <LoadingOutlined
        style={{ fontSize: '48px' }}
        className='animate-spin text-primary'
      />
    </div>
  ) : (
    <>
      <div className='bg-white w-full rounded'>
        <header className='flex justify-between m-0'>
          <h6 className='font-medium text-[18px]'>Pengaturan Profil Akun</h6>
        </header>
        <div className='m-0 w-full h-[1px] bg-gradient-to-r from-[#00000050] to-transparent'></div>
        <div className='mt-8 '>
          <Form action='#' id='user_profile' className=' flex flex-row gap-8'>
            <div className='flex flex-col w-2/3 gap-4'>
              <div className=''>
                <label className='font-medium text-[14px]' for='username'>
                  Username
                </label>
                <InputLogged
                  type='text'
                  id='username'
                  placeholder={user.username}
                />
              </div>
              <div className='flex flex-row gap-4'>
                <div className='w-1/2'>
                  <label className='font-medium text-[14px]' for='firstName'>
                    Nama Depan
                  </label>
                  <InputLogged
                    type='text'
                    id='firstName'
                    placeholder={user.firstName}
                  />
                </div>
                <div className='w-1/2'>
                  <label className='font-medium text-[14px]' for='lastName'>
                    Nama Belakang
                  </label>
                  <InputLogged
                    type='text'
                    id='lastName'
                    placeholder={user.lastName}
                  />
                </div>
              </div>
              <div className=''>
                <label className='font-medium text-[14px]' for='email'>
                  Email
                </label>
                <InputLogged type='text' id='email' placeholder={user.email} />
              </div>
              <div className=''>
                <label className='font-medium text-[14px]' for='phone'>
                  Nomor Telepon
                </label>
                <InputLogged
                  type='text'
                  id='phone'
                  placeholder={user.phoneNumb}
                />
              </div>
              <div className='flex flex-col'>
                <label className='font-medium text-[14px]' for='gender'>
                  Gender
                </label>
                {/* <Select form='user_profile' id='gender' name='genders'>
                  <option value='' selected hidden>
                    {user.gender}
                  </option>
                  <option value='pria'>pria</option>
                  <option value='wanita'>wanita</option>
                </Select> */}
                <AntdSelect
                  placeholder={user.gender}
                  // showArrow={false}
                  onChange={handleGender}
                  className='bg-light text-black font-[14px] border-darkTrans border-[1px] rounded cursor-text'
                >
                  <Option value='Laki - laki'>Laki - laki</Option>
                  <Option value='Perempuan'>Perempuan</Option>
                </AntdSelect>
              </div>
              <div className='flex flex-col'>
                <label className='font-medium text-[14px]' for='birthdate'>
                  Tanggal Lahir
                </label>
                <DatePicker
                  className='bg-light text-black font-[14px] border-darkTrans border-[1px] rounded'
                  defaultValue={moment(
                    user.birthDate ? user.birthDate : '01/01/1970'
                  )}
                  format={dateFormat}
                />
              </div>
              <div className='flex flex-row gap-4'>
                <div className='w-1/2'>
                  <label className='font-medium text-[14px]' for='province'>
                    provinsi
                  </label>
                  <Select
                    form='user_profile'
                    id='province'
                    name='province'
                    onChange={(e) => handleSelectChange(e.target.value)}
                  >
                    <option value='' selected disabled hidden>
                      {user.location?.province}
                    </option>
                    {location.map((v, i) => {
                      return (
                        <>
                          <option key={i} value={v.provinsi}>
                            {v.provinsi}
                          </option>
                        </>
                      );
                    })}
                  </Select>
                </div>
                <div className='w-1/2'>
                  <label className='font-medium text-[14px]' for='city'>
                    Kota / Kabupaten
                  </label>
                  <Select form='user_profile' id='city' name='city'>
                    <option value='' selected disabled hidden>
                      {user.location?.city}
                    </option>

                    {location
                      .find((item) => item.provinsi === prov)
                      .kota.map((kot, i) => {
                        return (
                          <option key={i} value={kot}>
                            {kot}
                          </option>
                        );
                      })}
                  </Select>
                </div>
              </div>
              <div className='w-full'>
                <label className='font-medium text-[14px]' for='address'>
                  Alamat
                </label>
                <Input.TextArea
                  defaultValue={user.location?.address}
                  showCount
                  maxLength={100}
                  bordered={false}
                  style={{
                    minHeight: 20,
                  }}
                  className='border border-dark rounded-md p-[1px]'
                  onChange={onChange}
                />
              </div>
              <div className='mt-2'>
                <input
                  type='submit'
                  value='SIMPAN'
                  className='bg-primary w-full text-light font-medium h-[36px] rounded cursor-pointer'
                />
              </div>
            </div>
            <div className='flex flex-col gap-4 w-1/3'>
              <label className='font-medium text-[14px]' for='img-profile'>
                Foto Profil
              </label>
              <div className='w-min h-min'>
                <Avatar
                  src={user.profilePhoto}
                  size={{ xs: 24, sm: 32, md: 40, lg: 64, xl: 100, xxl: 160 }}
                  className=' bg-primaryTrans'
                >
                  <span style={{ fontSize: '3rem' }}>
                    {user.profilePhoto
                      ? ''
                      : user.username.charAt(0).toUpperCase()}
                  </span>
                </Avatar>
              </div>
              {/* <input type="file" className="bg-primary w-full text-light font-medium h-[36px] rounded cursor-pointer"/> */}
              <div className='w-full'>
                <Upload listType='picture' className='w-full' {...propsUpload}>
                  <button
                    className='px-10 h-[32px] bg-primary text-light rounded'
                    block
                  >
                    Pilih Foto Profil
                  </button>
                </Upload>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </>
  );
};

export default ClientProfile;

ClientProfile.getLayout = function Layout(page) {
  return <ClientLayout>{page}</ClientLayout>;
};
