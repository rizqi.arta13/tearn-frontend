import ClientLayout from 'components/Layout/ClientLayout';
import { useEffect, useState } from 'react';
import { LoadingOutlined } from '@ant-design/icons';
import Chip from 'components/build/Chip';
import Button from 'components/build/Button';
import { Modal, Button as AntButton, Pagination, notification } from 'antd';
import { format } from 'date-fns';
import InputSearchFilter from 'components/build/InputSearchFilter';
import { Search } from 'akar-icons';
import { transactionRepository } from 'repository/transaction';
import { mutate } from 'swr';

const openNotificationWithIcon = (message, description) => {
  notification.error({
    message,
    description,
  });
};

const Dashboard = ({ children }) => {
  const [data, setData] = useState([]);
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [search, setSearch] = useState('');
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState([]);

  const { data: fetchData } = transactionRepository.hooks.useOrder({
    page: current,
    limit: pageSize,
    search,
  });

  const handlePagination = async (page) => {
    setCurrent(page);
  };

  const handleSearch = (e) => {
    setSearch(e.target.value);
  };

  useEffect(() => {
    if (fetchData !== null && fetchData !== undefined) {
      console.log(fetchData);
      setData(fetchData.result[0]);
      setCount(fetchData.result[1]);
      setTimeout(() => {
        setLoading(false);
      }, 500);
    }
  }, [fetchData]);

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const col = ['Paket', 'Mentor', 'Status', 'Aksi'];

  const handleDissaprove = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await transactionRepository.manipulateData.orderUpdate(id, {
        status: 'dibatalkan',
      });
      mutate(
        transactionRepository.url.order({
          page: current,
          limit: pageSize,
          search,
        })
      );
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    } finally {
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  return loading ? (
    <>
      <div className='w-full min-h-[500px] flex items-center justify-center animate-pulse text-primary'>
        <LoadingOutlined
          style={{ fontSize: '48px' }}
          className='animate-spin text-primary'
        />
      </div>
    </>
  ) : (
    <>
      <div className='bg-white w-full rounded'>
        <div className='flex justify-between m-0'>
          <h6 className='font-medium text-[18px]'>Pembelajaran Berlangsung</h6>
          <div className='flex items-center'>
            <div className='relative left-7'>
              <Search strokeWidth={2} size={18} />
            </div>
            <InputSearchFilter
              className='pl-[36px] placeholder:text-darkTrans'
              placeholder='Cari Paket'
              onChange={handleSearch}
            />
          </div>
        </div>
        <div className='mt-4 '>
          <table className='w-full text-left mb-9'>
            <thead>
              <tr className='bg-primaryLight h-10 border-b border-b-[rgba(0 0 0 0.5)] table-row'>
                {col.map((v, i) => {
                  return (
                    <th key={i} className='px-5 font-medium py-3 table-cell'>
                      {v}
                    </th>
                  );
                })}
              </tr>
            </thead>
            <tbody>
              {data.map((v) => {
                let type;
                let action;
                switch (v.status) {
                  case 'berjalan':
                    type = 'primary';
                    action = [
                      <AntButton
                        key='Block'
                        type='primary'
                        danger
                        onClick={() => handleDissaprove(v.id)}
                      >
                        Tolak
                      </AntButton>,
                      <AntButton
                        key='back'
                        type='primary'
                        onClick={() => handleApprove(v.id)}
                      >
                        Terima
                      </AntButton>,
                    ];
                    break;
                  case 'selesai':
                    type = 'active';
                    action = [
                      <AntButton key='Block' onClick={() => handleCancel(v.id)}>
                        kembali
                      </AntButton>,
                    ];
                    break;
                  case 'menunggu pembayaran':
                  case 'menunggu konfirmasi':
                    type = 'warning';
                    action = [
                      <AntButton
                        key='Block'
                        type='primary'
                        danger
                        onClick={() => handleDissaprove(v.id)}
                      >
                        Tolak
                      </AntButton>,
                      <AntButton
                        key='back'
                        type='primary'
                        href='/client/payment'
                      >
                        Ke Pembayaran
                      </AntButton>,
                    ];
                    break;
                  case 'dibatalkan':
                    type = 'danger';
                    action = [
                      <AntButton key='Block' onClick={() => handleCancel(v.id)}>
                        kembali
                      </AntButton>,
                    ];
                    break;
                  default:
                    type = 'disabled';
                    break;
                }

                return (
                  <tr
                    key={v.id}
                    className='pb-3 h-10 border-b border-b-[rgba(0 0 0 0.5)]'
                  >
                    <td className='px-4 py-3'>{v.gig.title}</td>
                    <td className='px-4 py-3'>
                      {v.gig.mentor.user.firstName} {v.gig.mentor.user.lastName}
                    </td>
                    <td className='px-4 py-3'>
                      <Chip type={type}>{v.status}</Chip>
                    </td>
                    <td className='px-4 py-3'>
                      <Button className='px-4' onClick={() => showModal(v.id)}>
                        Lihat
                      </Button>
                      <Modal
                        key={v.id}
                        visible={isModalVisible[v.id]}
                        onOk={() => handleOk(v.id)}
                        onCancel={() => handleCancel(v.id)}
                        className='p-9'
                        width={560}
                        footer={action}
                      >
                        <div className=''>
                          <h3 className='font-medium text-[18px] mb-6'>
                            {v.gig.title}
                          </h3>
                          <div className='w-full flex gap-2 mb-2'>
                            <div className='w-5/12 font-medium text-[14px]'>
                              Nama Mentor
                            </div>
                            <div className='w-7/12 text-[14px]'>
                              {v.gig.mentor.user.firstName}{' '}
                              {v.gig.mentor.user.lastName}
                            </div>
                          </div>
                          <div className='w-full flex gap-2'>
                            <div className='w-5/12 font-medium text-[14px]'>
                              Mata Pelajaran
                            </div>
                            <div className='w-7/12 text-[14px] mb-2'>
                              <ul className='text-[14px]'>
                                {v.gig.subjects.map((v, i) => {
                                  return (
                                    <li key={i}>
                                      {'- '}
                                      {v.subjectId.subject}
                                    </li>
                                  );
                                })}
                              </ul>
                            </div>
                          </div>
                          <div className='w-full flex gap-2 mb-2'>
                            <div className='w-5/12 font-medium text-[14px]'>
                              Estimasi Waktu
                            </div>
                            <div className='w-7/12 text-[14px]'>
                              <span className=''>{v.startDate} </span>
                              {'  -  '}
                              <span className=''>{v.endDate}</span>
                            </div>
                          </div>
                          <div className='w-full flex gap-2 mb-2'>
                            <div className='w-5/12 font-medium text-[14px]'>
                              Estimasi Jam
                            </div>
                            <div className='w-7/12 text-[14px]'>
                              <span className=''>
                                {format(new Date(v.startHour), 'HH:mm')}{' '}
                              </span>
                              {' - '}
                              <span className=''>
                                {format(new Date(v.endHour), 'HH:mm')}
                              </span>
                            </div>
                          </div>
                          <div className='w-full flex gap-2 mb-2'>
                            <div className='w-5/12 font-medium text-[14px]'>
                              Status
                            </div>
                            <div className='w-7/12 text-[14px]'>
                              <Chip type={type}>{v.status}</Chip>
                            </div>
                          </div>
                        </div>
                      </Modal>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <div className='flex justify-center'>
            <Pagination
              current={current}
              onChange={handlePagination}
              total={count}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Dashboard;

Dashboard.getLayout = function Layout(page) {
  return <ClientLayout>{page}</ClientLayout>;
};
