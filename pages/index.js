import Navbar from 'components/build/Navbar';
import Subjects from 'app/subjects';
import Testimony from 'app/testimony';
import { Image } from 'antd';
import Hero from '../components/build/Hero';
import Info from 'app/info';
import Footer from 'components/build/Footer';

const Home = () => {
  return (
    <div className='relative'>
      <Navbar logged={false} active={'Beranda'} />
      <div className="relative flex flex-col top-[70px] min-h-screen">
        <Hero />
        <div>
          <Info />
          <Subjects />
        </div>
        <Testimony />
        <div className='pb-80 pt-20'>
          <h3 className='text-5xl text-center m-0'>
            <strong>
              Cobain
              <Image
                src='/logo.svg'
                alt='tearn logo'
                preview={false}
                className='px-2 cursor-pointer'
              />
              Sekarang
            </strong>
          </h3>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Home;
