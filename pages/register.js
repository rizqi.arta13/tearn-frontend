import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react-lite';
import {
  Row,
  Col,
  Typography,
  Form,
  Radio,
  Space,
  Input,
  notification,
} from 'antd';
import Logo from '../components/build/Logo.js';
import Button from '../components/build/Button.js';
import Link from 'next/link.js';
import { userRepository } from 'repository/user.js';
import { useRouter } from 'next/router.js';

const openNotificationWithIcon = (message, description) => {
  notification.error({
    message,
    description,
  });
};

const Register = observer(() => {
  const [status, setStatus] = useState('ditolak');
  const [isActive, setIsActive] = useState(false);
  const router = useRouter();

  const handleChangeStatusByRole = async (e) => {
    const value = await e.target.value;

    if (value === 'client') {
      setStatus('diterima');
      setIsActive(true);
    } else if (value === 'mentor') {
      setStatus('menunggu');
      setIsActive(false);
    }

    console.log(value, isActive, status);
  };

  const onFinish = async (values) => {
    try {
      const response = await userRepository.manipulateData.register({
        ...values,
        isActive,
        status,
      });

      if (response.statusCode !== 500) {
        response.body;
        localStorage.setItem('token', response.body.token);
        switch (values.role) {
          case 'client':
            router.push('/client');
            break;
          case 'mentor':
            router.push('/mentor');
            break;
        }
      }
    } catch (err) {
      console.log(err);
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
      // if (err.response.) {
      //   openNotificationWithIcon(err.response.body.message);
      // }
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div
      style={{
        width: '100vw',
        display: 'flex',
        justifyContent: 'center',
        height: '100vh',
        alignItems: 'center',
      }}
    >
      <Row justify={'center'}>
        <Col>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-start',
              marginTop: '1vh',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div
              style={{ width: 352, height: 560, textAlign: 'center' }}
              className={'shadow rounded-[20px] bg-light p-[24px]'}
            >
              <div className='flex justify-center mx-auto my-3 mb-8'>
                <Logo />
              </div>

              <Form
                layout={'vertical'}
                name='normal_login'
                className='login-form'
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
              >
                <Form.Item
                  style={{
                    marginBottom: '24px',
                  }}
                  name='email'
                  size={'large'}
                  rules={[
                    { required: false, message: 'Masukkan Email!' },
                    { type: 'email', message: 'Email tidak benar!' },
                  ]}
                >
                  <Input
                    bordered={false}
                    placeholder='Email'
                    className='px-3 py-2 w-[256px] h-[32px] bg-primaryLight text-black border-none border-2 rounded placeholder:text-placeholder focus:outline-none'
                  />
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: '24px',
                  }}
                  name='username'
                  size={'large'}
                  rules={[
                    { required: true, message: 'Username tidak boleh kosong' },
                    {
                      min: 6,
                      message: 'Gunakan 6 karakter atau lebih untuk username',
                    },
                  ]}
                >
                  <Input
                    type='text'
                    bordered={false}
                    placeholder='Username'
                    className='px-3 py-2 w-[256px] h-[32px] bg-primaryLight text-black border-none border-2 rounded placeholder:text-placeholder focus:outline-none'
                  />
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: '24px',
                  }}
                  name='password'
                  size={'large'}
                  rules={[
                    {
                      min: 8,
                      message: 'Gunakan 8 karakter atau lebih untuk sandi',
                    },
                    {
                      required: true,
                      message: 'Masukkan sandi anda!',
                    },
                  ]}
                >
                  <Input.Password
                    bordered={false}
                    placeholder='Password'
                    className='px-3 py-2 w-[256px] h-[32px] bg-primaryLight text-black border-none border-2 rounded placeholder:text-placeholder focus:outline-none'
                  />
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: '24px',
                  }}
                  name='confPassword'
                  size={'large'}
                  rules={[
                    {
                      min: 8,
                      message: 'Gunakan 8 karakter atau lebih untuk sandi',
                    },
                    {
                      required: true,
                      message: 'Konfirmasi sandi anda!',
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue('password') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          new Error('Password tidak sesuai!')
                        );
                      },
                    }),
                  ]}
                >
                  <Input.Password
                    bordered={false}
                    placeholder='Konfirmasi password'
                    className='px-3 py-2 w-[256px] h-[32px] bg-primaryLight text-black border-none border-2 rounded placeholder:text-placeholder focus:outline-none'
                  />
                </Form.Item>
                <Form.Item
                  name='role'
                  rules={[
                    {
                      required: true,
                      message: 'Pilih salah satu!',
                    },
                  ]}
                >
                  <Radio.Group
                    name='role'
                    style={{
                      marginBottom: '24px',
                    }}
                    onChange={handleChangeStatusByRole}
                  >
                    <Space direction='horizontal'>
                      <Typography.Paragraph
                        style={{
                          margin: 0,
                          padding: 0,
                          fontSize: 12,
                          marginRight: 36,
                          fontWeight: 400,
                        }}
                        className='dark font-bold'
                      >
                        Pilih Role
                      </Typography.Paragraph>
                      <Radio value={'client'}>Klien</Radio>
                      <Radio value={'mentor'}>Mentor</Radio>
                      {/* <Radio>
                                    {value === 2 ? (
                                        <Input
                                        style={{
                                            width: 100,
                                            marginLeft: 10,
                                        }}
                                        />
                                    ) : null}
                                    </Radio> */}
                    </Space>
                  </Radio.Group>
                </Form.Item>

                <Form.Item className='hidden' name='status'>
                  <Input type='hidden' value={status} />
                </Form.Item>
                <Form.Item  className='hidden' name='isActive'>
                  <Input type='hidden' value={isActive} />
                </Form.Item>
                <Form.Item
                  style={{
                    marginBottom: '24px',
                    fontWeight: 'bold',
                  }}
                >
                  <Button className='h-[32px] w-[132px] bg-primary text-light rounded'>
                    Daftar
                  </Button>
                </Form.Item>
                <div className='mt-16'>
                  Sudah punya punya akun? Masuk{' '}
                  <Link href='/login' className='primary'>
                    disini!
                  </Link>
                </div>
              </Form>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
});

export default Register;
