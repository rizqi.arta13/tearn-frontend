export const data = [
  {
    id: 1,
    mentor: {
      user: {
        firstName: 'Johny',
        lastName: 'di Caprio',
        email: 'john.sins66@gmail.com',
        phone: '089696966666',
        profilePhoto: 'https://thispersondoesnotexist.com/image',
      },
    },
    title: 'Paket UN mantap',
    schedules: ['senin', 'selasa', 'kamis'],
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maxime, saepe. Amet, eum repellendus? Inventore architecto officia accusamus eligendi! Necessitatibus, dicta!',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    harga: 50000,
    rate: 4.9,
  },
  {
    id: 2,
    mentor: {
      user: {
        firstName: 'Jordi',
        lastName: 'di Caprio',
        email: 'john.sins66@gmail.com',
        phone: '089696966666',
        profilePhoto: 'https://thispersondoesnotexist.com/image',
      },
    },
    title: 'Belajar Baca Bismillah',
    schedules: ['senin', 'selasa', 'kamis'],
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maxime, saepe. Amet, eum repellendus? Inventore architecto officia accusamus eligendi! Necessitatibus, dicta!',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    harga: 50000,
    rate: 4.9,
  },
  {
    id: 3,
    mentor: {
      user: {
        firstName: 'Amad',
        lastName: 'Jordi',
        email: 'john.sins66@gmail.com',
        phone: '089696966666',
      },
    },
    title: 'Paket Yoga dan Programing with Domino QiuQiu',
    schedules: ['senin', 'selasa', 'kamis'],
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maxime, saepe. Amet, eum repellendus? Inventore architecto officia accusamus eligendi! Necessitatibus, dicta!',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    harga: 50000,
    rate: 4.9,
  },
  {
    id: 4,
    mentor: {
      user: {
        firstName: 'Satoru',
        lastName: 'Gojou',
        email: 'john.sins66@gmail.com',
        phone: '089696966666',
        profilePhoto: 'https://thispersondoesnotexist.com/image',
      },
    },
    title: 'Paket Unch Mantap',
    schedules: ['senin', 'selasa', 'kamis'],
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maxime, saepe. Amet, eum repellendus? Inventore architecto officia accusamus eligendi! Necessitatibus, dicta!',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    harga: 50000,
    rate: 4.9,
  },
  {
    id: 5,
    mentor: {
      user: {
        firstName: 'Johny',
        lastName: 'di Caprio',
        email: 'john.sins66@gmail.com',
        phone: '089696966666',
      },
    },
    title: 'Paket UNAIR',
    schedules: ['senin', 'selasa', 'kamis'],
    desc: 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Maxime, saepe. Amet, eum repellendus? Inventore architecto officia accusamus eligendi! Necessitatibus, dicta!',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    harga: 50000,
    rate: 4.9,
  },

];
