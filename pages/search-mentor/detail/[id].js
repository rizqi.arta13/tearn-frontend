import { MailFilled, PhoneFilled } from '@ant-design/icons';
import { Avatar, Divider, Rate } from 'antd';
import Button from 'components/build/Button';
import GigsCard from 'components/build/GigsCard';
import DetailMentorLayout from 'components/Layout/DetailMentorLayout';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { gigRepository } from 'repository/gigs';
import { data } from './data';

const GigsDetail = () => {
  const router = useRouter();
  const route = useRouter();
  const { id } = route.query;
  const { data: fetchData } = gigRepository.hooks.useGigDetail(id);
  console.log(fetchData);
  const { data: gigsByMentor } = gigRepository.hooks.useGigs({
    mentor: fetchData.mentor,
    limit: 5,
    page: 1,
  });
  // console.log(gigsByMentor);

  return (
    <>
      <div className='flex items-center justify-center px-36'>
        <div className='w-full flex gap-10'>
          <div className='min-w-[400px] max-w-full flex flex-col p-8 bg-light shadow-lg rounded-md gap-8'>
            <h1 className='text-[18px]'>Detail Jasa</h1>
            <div className='flex gap-4'>
              <div className='flex items-center'>
                <Avatar
                  size={128}
                  src={fetchData?.mentor.user.profilePhoto}
                  className='bg-primaryTrans'
                >
                  {fetchData?.mentor.user.profilePhoto
                    ? ''
                    : fetchData?.mentor.user.username.charAt(0).toUpperCase()}
                </Avatar>
              </div>
              <div className='w-full flex flex-col justify-center'>
                <p className='font-medium text-[14px] m-0'>{`${fetchData?.mentor.user.firstName} ${fetchData?.mentor.user.lastName}`}</p>
                <h3 className='leading-[32px] text-primary text-[28px] m-0'>
                  {fetchData?.title}
                </h3>
                <div className='m-0 flex mt-1'>
                  <Avatar
                    className='w-[16px] h-[16px] mr-1'
                    src={'/img/icons/star.svg'}
                    shape='square'
                  />
                  <p className='font-medium m-0 text-[12px]'>
                    4.6{' '}
                    <Link href={'/#'}>
                      <a>(20 penilaian)</a>
                    </Link>
                  </p>
                </div>
              </div>
            </div>
            <div className='flex flex-col mb-6 w-full'>
              <h1 className='text-[16px]'>Tentang Jasa Ini</h1>
              <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />
              <div className='flex flex-row pt-8 gap-4'>
                <div className='w-1/3 font-medium'>Deskripsi</div>
                <div className='w-full'>{fetchData?.description}</div>
              </div>
              <div className='flex flex-row pt-8 gap-4'>
                <div className='w-1/3 font-medium'>Ketentuan</div>
                <ul className='w-full list-disc ml-6'>
                  <li>Hubungi sebelum membayar</li>
                  <li>Bayar dimuka</li>
                  <li>Mentoring via daring</li>
                  <li>
                    Pembatalan pembelajaran dilakukan 2 jam sebelum jam
                    pelajaran dimulai
                  </li>
                </ul>
              </div>
              <div className='flex flex-row pt-8  gap-4'>
                <div className='w-1/3 font-medium'>Kontak</div>
                <ul className='w-full'>
                  <li className='flex items-center text-primary gap-2'>
                    <MailFilled /> {fetchData?.mentor.user.email}
                  </li>
                  <li className='flex items-center text-primary gap-2'>
                    <PhoneFilled /> {fetchData?.mentor.user.phoneNumb}
                  </li>
                </ul>
              </div>
              <div className='flex flex-row pt-8  gap-4'>
                <div className='w-1/3 font-medium'>Ketersediaan</div>
                <ul className='w-full list-disc ml-6'>
                  {fetchData?.mentor.availability.map((v, i) => {
                    return (
                      <li key={i} className='flex justify-between w-1/3'>
                        <span>{v.day}</span>
                        <span>
                          {v.startTime}
                          {' - '}
                          {v.startTime}
                        </span>
                      </li>
                    );
                  })}
                </ul>
              </div>
              <div className='flex flex-row pt-8  gap-4'>
                <div className='w-1/3 font-medium'>Mata Pelajaran</div>
                <ul className='w-full list-disc ml-6'>
                  {fetchData?.subjects.map((v, i) => {
                    return (
                      <li key={i}>
                        <span>{v.subjectId.subject}</span>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </div>
            <div className='flex flex-col w-full'>
              <h1 className='text-[16px] m-0'>Penilaian</h1>
              <div className='flex items-center gap-4'>
                <Rate disabled allowHalf defaultValue={4.5} />
                <p className='m-0'>
                  <span className='font-semibold'>4.7 </span>(dari 20 ulasan)
                </p>
              </div>
              <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />

              <div className='w-full flex pt-6'>
                <div className='w-5/12 flex gap-4'>
                  <div className='flex items-center'>
                    <Avatar size={40} className='bg-primaryTrans'>
                      A
                    </Avatar>
                  </div>
                  <div className='w-full flex flex-col justify-center'>
                    <p className='font-medium m-0 mb-1 text-[12px] leading-[12px]'>
                      Alexis Messi
                    </p>
                    <p className='m-0 text-[12px] leading-[12px]'>
                      12 November 2022
                    </p>
                  </div>
                </div>
                <div className='w-full flex flex-col'>
                  <Rate
                    disabled
                    allowHalf
                    defaultValue={4.5}
                    className='text-sm'
                  />
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Pulvinar et dui, cursus amet, orci, phasellus tincidunt.
                    Ullamcorper faucibus vestibulum, vitae in pretium diam non
                    egestas. Viverra lectus non mi vel. Massa hendrerit gravida
                    sed est elit quis arcu magna felis.
                  </p>
                </div>
              </div>
            </div>
            <div className='flex flex-col w-full'>
              <h1 className='text-[16px] mt-12'>
                Paket Lainnya dari {fetchData?.mentor.user.firstName}
              </h1>
              <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />
              <div className='flex flex-col gap-6 pt-6'>
                {data.map((v, i) => {
                  return (
                    <>
                      <Link
                        key={i}
                        href={router.pathname + '?id=' + v.id}
                        passHref
                      >
                        <a>
                          <GigsCard
                            image={v.mentor.user.profilePhoto}
                            noImage={
                              v.mentor.user.profilePhoto
                                ? ''
                                : v.mentor.user.firstName
                                    .charAt(0)
                                    .toUpperCase()
                            }
                            gigsTitle={v.title}
                            rating={v.rate}
                            price={v.harga}
                            desc={v.desc}
                            mentor={[
                              v.mentor.user.firstName,
                              v.mentor.user.lastName,
                            ]}
                          />
                        </a>
                      </Link>
                    </>
                  );
                })}
              </div>
            </div>
          </div>
          <div className='max-w-[320px] w-full h-min sticky top-[calc(80px+10px)]'>
            <div className='w-full bg-light rounded-md shadow-lg flex flex-col'>
              <p className='m-0 p-3'>
                <span className='font-medium text-[18px]'>
                  {new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                  }).format(fetchData?.amount)}{' '}
                </span>{' '}
                /jam
              </p>
              <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />
              <p className='mr-4 ml-4 mb-0 mt-6 text-[12px] font-semibold'>
                Ketentuan
              </p>
              <ul className='ml-8 mr-4 list-disc text-ellipsis leading-loose text-justify'>
                <li>Bayar dimuka</li>
                <li>Mentoring via daring</li>
                <li>
                  pembatalan dilakukan 2 jam sebelum jam pelajaran dimulai
                </li>
              </ul>
              <div className='p-4'>
                <Button className='w-full h-[40px] m-auto text-[16px] font-semibold'>
                  Rekrut
                </Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default GigsDetail;

GigsDetail.getLayout = function Layout(page) {
  return <DetailMentorLayout>{page}</DetailMentorLayout>;
};
