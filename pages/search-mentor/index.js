import { LoadingOutlined } from '@ant-design/icons';
import { Empty } from 'antd';
import GigsCard from 'components/build/GigsCard';
import SearchMentorLayout, {
  SearchMentorContext,
} from 'components/Layout/SearchMentorLayout';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import { gigRepository } from 'repository/gigs';

const SearchMentorPage = () => {
  const router = useRouter();
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [loading, setLoading] = useState(true);
  const filter = useContext(SearchMentorContext);

  const { data: fetchData } = gigRepository.hooks.useSearchGigs(filter);

  useEffect(() => {
    setTimeout(() => {
      if (fetchData) {
        setData(fetchData.data);
        setCount(fetchData.count);
      }
      setLoading(false);
    }, 500);
  }, [fetchData]);

  return loading ? (
    <div className='w-full h-full flex items-center justify-center animate-pulse text-primary'>
      <LoadingOutlined
        style={{ fontSize: '48px' }}
        className='animate-spin text-primary'
      />
    </div>
  ) : count <= 0 ? (
    <div className='w-full h-full flex justify-center items-center'>
      <Empty description='Kamu Belum Cari Paket Pembelajaran' />
    </div>
  ) : (
    <>
      <h1 className='text-[18px]'>Hasil</h1>
      {data.map((v, i) => {
        return (
          <React.Fragment key={v.id}>
            <Link key={i} href={router.pathname + '/detail/' + v.id} passHref>
              <a>
                <GigsCard
                  image={v.mentor.user.profilePhoto}
                  noImage={
                    v.mentor.user.profilePhoto
                      ? ''
                      : v.mentor.user.firstName.charAt(0).toUpperCase()
                  }
                  gigsTitle={v.title}
                  rating={v.rate}
                  price={v.amount}
                  desc={v.desc}
                  mentor={[v.mentor.user.firstName, v.mentor.user.lastName]}
                />
              </a>
            </Link>
          </React.Fragment>
        );
      })}
    </>
  );
};

export default SearchMentorPage;

SearchMentorPage.getLayout = function Layout(page) {
  return <SearchMentorLayout>{page}</SearchMentorLayout>;
};
