import '../styles/globals.css';
import 'tailwindcss/tailwind.css';
import { StoreProvider } from '../components/StoreProvider';
import MentorLayout from 'components/Layout/MentorLayout';
import AdminLayout from 'components/Layout/AdminLayout';
import ClientLayout from 'components/Layout/ClientLayout';

function MyApp({ Component, pageProps, router }) {
  // Use the layout defined at the page level, if available
  const getLayout = Component.getLayout || ((page) => page);

  if (router.pathname.startsWith('/client')) {
    return (
      <ClientLayout>
        <Component {...pageProps} />
      </ClientLayout>
    );
  }

  if (router.pathname.startsWith('/mentor')) {
    return (
      <MentorLayout>
        <Component {...pageProps} />
      </MentorLayout>
    );
  }

  if (router.pathname.startsWith('/admin')) {
    return (
      <AdminLayout>
        <Component {...pageProps} />
      </AdminLayout>
    );
  }

  return getLayout(
    <StoreProvider {...pageProps}>
      <Component {...pageProps} />
    </StoreProvider>
  );
}

export default MyApp;
