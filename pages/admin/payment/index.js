import {
  Avatar,
  Modal,
  Spin,
  Button as AntButton,
  Image,
  Pagination,
} from 'antd';
import Chip from 'components/build/Chip';
import Button from 'components/build/Button';
import { useEffect, useState } from 'react';
import { format } from 'date-fns';
import { transactionRepository } from 'repository/transaction';
import { mutate } from 'swr';

const columns = [
  {
    id: 1,
    name: 'nama paket pesanan',
    title: 'Nama Paket Pesanan',
  },
  {
    id: 2,
    name: 'pemilik paket',
    title: 'Pemilik Paket',
  },
  {
    id: 3,
    name: 'pemesan',
    title: 'Pemesan',
  },
  {
    id: 6,
    name: 'tanggal pemesanan',
    title: 'Tanggal Pemesanan',
  },
  {
    id: 4,
    name: 'status',
    title: 'Status',
  },
  {
    id: 5,
    name: 'aksi',
    title: 'Aksi',
  },
];

const Payment = () => {
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [current, setCurrent] = useState(1);
  const [loading, setLoading] = useState(true);
  const [pageSize, setPageSize] = useState(10);
  const [isModalVisible, setIsModalVisible] = useState([]);

  const { data: fetchData } = transactionRepository.hooks.usePayment({
    page: current,
    limit: pageSize,
  });

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleApprove = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await transactionRepository.manipulateData
        .approve(id, {
          status: 'diterima',
        })
        .then(async () => {
          await transactionRepository.manipulateData.orderUpdate(id, {
            status: 'berjalan',
          });
        })
        .catch((err) => {
          console.log(err);
        });
      mutate(
        transactionRepository.url.payment({
          page: current,
          limit: pageSize,
        })
      );
    } catch (err) {
      console.log(err);
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    } finally {
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };
  const handleDissaprove = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await transactionRepository.manipulateData.approve(id, {
        status: 'ditolak',
      });
      mutate(
        transactionRepository.url.payment({
          page: current,
          limit: pageSize,
        })
      );
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    } finally {
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  const handlePagination = async (page) => {
    setCurrent(page);
  };
  useEffect(() => {
    if (fetchData !== null && fetchData !== undefined) {
      setData(fetchData.data);
      setCount(fetchData.count);
      setLoading(false);
    }
  }, [fetchData]);

  return (
    <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
      {loading ? (
        <div className='flex flex-col justify-center h-full'>
          <Spin />
        </div>
      ) : (
        <>
          <header className='flex justify-between mb-3 '>
            <h6 className='font-semibold text-2xl'>Daftar Pemesanan</h6>
          </header>
          <div>
            <table className='w-full text-left mb-9'>
              <thead>
                <tr className='bg-primaryLight h-10 border-b border-b-[rgba(0 0 0 0.5)] table-row'>
                  {columns.map((col) => {
                    return (
                      <th key={col.id} className='px-5 py-3 table-cell'>
                        {col.title}
                      </th>
                    );
                  })}
                </tr>
              </thead>
              <tbody>
                {data.map((v) => {
                  let type;
                  let button;

                  switch (v.status) {
                    case 'selesai':
                    case 'diterima':
                    case 'dibayarkan dengan guru':
                      type = 'active';
                      button = [
                        <AntButton
                          key='back'
                          onClick={() => handleCancel(v.id)}
                        >
                          Kembali
                        </AntButton>,
                        <AntButton
                          key='Block'
                          type='primary'
                          primary
                          href={
                            process.env.NEXT_PUBLIC_BASE_URL + '/pdf/' + v.id
                          }
                        >
                          Unduh
                        </AntButton>,
                      ];
                      break;
                    case 'menunggu':
                    case 'menunggu konfirmasi':
                      type = 'warning';
                      button = [
                        <AntButton
                          key='back'
                          onClick={() => handleCancel(v.id)}
                        >
                          Kembali
                        </AntButton>,
                        <AntButton
                          key='Block'
                          type='primary'
                          danger
                          onClick={() => handleDissaprove(v.id)}
                        >
                          Tolak
                        </AntButton>,
                        <AntButton
                          key='Block'
                          type='primary'
                          className='bg-green-700'
                          onClick={() => handleApprove(v.id)}
                        >
                          Terima
                        </AntButton>,
                        <AntButton
                          key='Block'
                          type='primary'
                          primary
                          href={
                            process.env.NEXT_PUBLIC_BASE_URL + '/pdf/' + v.id
                          }
                        >
                          Unduh
                        </AntButton>,
                      ];
                      break;
                    case 'ditolak':
                      type = 'danger';
                      break;
                    case 'belum dibayar':
                    default:
                      type = 'disabled';
                      break;
                  }
                  return (
                    <tr key={v.id}>
                      <td className='px-5 py-3'>{v.order.gig.title}</td>
                      <td className='px-5 py-3'>
                        {v.order.gig.mentor.user.username}
                      </td>
                      <td className='px-5 py-3'>{v.order.user.username}</td>
                      <td className='px-5 py-3'>
                        {format(new Date(v.createdAt), 'dd-LLL-yyyy')}
                      </td>
                      <td className='px-5 py-3'>
                        <Chip type={type}>{v.status}</Chip>
                      </td>
                      <td className='px-5 py-3'>
                        <Button onClick={() => showModal(v.id)}>Lihat</Button>
                        <Modal
                          key={v.id}
                          visible={isModalVisible[v.id]}
                          onOk={() => handleOk(v.id)}
                          onCancel={() => handleCancel(v.id)}
                          className='p-9'
                          width={820}
                          footer={button}
                        >
                          <>
                            <p className='text-lg font-semibold'>Paket</p>
                            <div className='flex gap-6'>
                              <div className='w-full'>
                                <div className='flex justify-between items-center mb-3'>
                                  <span className='w-1/2 font-semibold'>
                                    Nama pemesan
                                  </span>
                                  <span className='w-1/2'>
                                    {v.order.user.username}
                                  </span>
                                </div>
                                <div className='flex justify-between items-center mb-3'>
                                  <span className='w-1/2 font-semibold'>
                                    Tanggal terjadwal
                                  </span>
                                  <span className='w-1/2'>
                                    {format(
                                      new Date(v.order.startDate),
                                      'dd/LL/yyyy'
                                    )}{' '}
                                    -{' '}
                                    {format(
                                      new Date(v.order.endDate),
                                      'dd/LL/yyyy'
                                    )}
                                  </span>
                                </div>
                                <div className='flex justify-between items-center mb-3'>
                                  <span className='w-1/2 font-semibold'>
                                    Jam terjadwal
                                  </span>
                                  <span className='w-1/2'>
                                    {format(
                                      new Date(v.order.startHour),
                                      'HH:mm'
                                    )}{' '}
                                    -{' '}
                                    {format(new Date(v.order.endHour), 'HH:mm')}
                                  </span>
                                </div>
                                <div className='flex justify-between items-center mb-3'>
                                  <span className='w-1/2 font-semibold'>
                                    status
                                  </span>
                                  <span className='w-1/2'>
                                    <Chip type={type}>{v.status}</Chip>
                                  </span>
                                </div>
                                <div className='flex justify-between items-center mb-3'>
                                  <span className='w-1/2 font-semibold'>
                                    Nama paket
                                  </span>
                                  <span className='w-1/2'>
                                    {v.order.gig.title}
                                  </span>
                                </div>
                                <div className='flex justify-between items-center mb-3'>
                                  <span className='w-1/2 font-semibold'>
                                    Mata Pelajaran
                                  </span>
                                  <ul className='w-1/2'>
                                    {v.order.gig.subjects.map((v, i) => {
                                      return <li key={i}>{v}</li>;
                                    })}
                                  </ul>
                                </div>
                                <div className='flex justify-between items-center mb-3'>
                                  <span className='w-1/2 font-semibold'>
                                    Harga
                                  </span>
                                  <span className='w-1/2 text-primary font-medium'>
                                    {new Intl.NumberFormat('id-ID', {
                                      style: 'currency',
                                      currency: 'IDR',
                                    }).format(v.order.amount)}
                                  </span>
                                </div>
                              </div>
                              <div className='w-1/2'>
                                <div className='font-semibold'>
                                  Bukti pembayaran
                                </div>
                                {console.log(v)}
                                <Image
                                  width={200}
                                  src={v.imageProofAdmin}
                                  alt='proof of payment'
                                />
                              </div>
                            </div>
                          </>
                        </Modal>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div className='flex justify-center'>
              <Pagination
                current={current}
                onChange={handlePagination}
                pageSize={pageSize}
                showTotal={(total, range) =>
                  `${range[0]}-${range[1]} of ${total} items`
                }
                onShowSizeChange={(current, size) => setPageSize(size)}
                total={count}
              />
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Payment;
