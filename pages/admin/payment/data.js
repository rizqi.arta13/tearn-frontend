export const data = [
  {
    id: 1,
    gigs: {
      id: 1,
      title: 'Paket UN mantap',
      subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
      price: 50000,
      rate: 4.6,
    },
    mentor: {
      id: 2,
      user: {
        username: 'kunyah',
        firstName: 'Udin',
        lastName: 'lucious',
      },
    },
    user: {
      username: 'odading',
      username: 'bodybody',
    },
    status: 'ditolak',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
    createdAt: '2022-07-20 13:53:53.194',
  },
  {
    id: 2,
    gigs: {
      id: 1,
      title: 'Paket UN mantap',
      subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
      price: 50000,
      rate: 4.6,
    },
    mentor: {
      id: 2,
      user: {
        username: 'odading',
        firstName: 'Udin',
        lastName: 'lucious',
      },
    },
    user: {
      username: 'arthentic',
      profile: 'https://thispersondoesnotexist.com/image',
    },
    status: 'selesai',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
    createdAt: '2022-07-22 13:53:53.194',
  },
  {
    id: 3,
    gigs: {
      id: 2,
      title: 'Makin dekat dengan yesus',
      subjects: ['1001 alkitab', 'Sejarah nasrani'],
      price: 50000,
      rate: 4.6,
    },
    mentor: {
      id: 1,
      user: {
        username: 'odading',
        firstName: 'Udin',
        lastName: 'lucious',
      },
    },
    user: {
      username: 'GapingDragon07',
    },
    status: 'menunggu',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
    createdAt: '2022-07-19 13:53:53.194',
  },
];
