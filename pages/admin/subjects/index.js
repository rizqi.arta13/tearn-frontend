import { PlusOutlined } from '@ant-design/icons';
import { Form, Input, notification, Spin } from 'antd';
import Button from 'components/build/Button';
import { useEffect, useRef, useState } from 'react';
import { gigRepository } from 'repository/gigs';
import { mutate } from 'swr';

const openNotificationWithIcon = (message, description) => {
  notification.error({
    message,
    description,
  });
};

const columns = [
  {
    id: 1,
    name: 'nama mata pelajaran',
    title: 'Nama Mata Pelajaran',
  },
  {
    id: 2,
    name: 'aksi',
    title: 'Aksi',
  },
];

const Subjects = () => {
  const ref = useRef();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [newSubjects, setNewSubjects] = useState('');

  const { data: fetchData } = gigRepository.hooks.useSubject();

  const handleCreateNewSubjects = async () => {
    try {
      console.log(newSubjects);
      await gigRepository.manipulateData.createSubject({
        subject: newSubjects,
      });
      mutate(gigRepository.url.getSubjects());
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    }
  };

  const handleDelete = async (id) => {
    try {
      await gigRepository.manipulateData.deleteSubject(id);
      mutate(gigRepository.url.getSubjects());
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    }
  };

  useEffect(() => {
    if (fetchData !== null && fetchData !== undefined) {
      console.log(fetchData);
      setData(fetchData.data);
      setLoading(false);
    }
  }, [fetchData]);

  return (
    <>
      <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
        {loading ? (
          <div className='flex flex-col justify-center h-full'>
            <Spin />
          </div>
        ) : (
          <>
            <header className='flex justify-between mb-3 '>
              <h6 className='font-semibold text-2xl'>Pengguna</h6>
            </header>
            <div>
              <div className='flex gap-4 mb-4'>
                <Input
                  ref={ref}
                  onChange={(e) => {
                    setNewSubjects(e.target.value);
                  }}
                  placeholder='Tambah Mata Pelajaran'
                />{' '}
                <Button type='priamry' onClick={handleCreateNewSubjects}>
                  <PlusOutlined />
                </Button>
              </div>
              <table className='w-full text-left mb-9'>
                <thead>
                  <tr className='bg-primaryLight h-10 border-b border-b-[rgba(0 0 0 0.5)] table-row'>
                    {columns.map((col) => {
                      return (
                        <th key={col.id} className='px-5 py-3 table-cell'>
                          {col.title}
                        </th>
                      );
                    })}
                  </tr>
                </thead>
                <tbody>
                  {data.map((v) => {
                    return (
                      <tr key={v.id}>
                        <td className='px-5 py-3'>{v.subject}</td>
                        <td className='px-5 py-3'>
                          <Button
                            type='danger'
                            onClick={() => handleDelete(v.id)}
                          >
                            hapus
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              <div className='flex justify-center'></div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default Subjects;
