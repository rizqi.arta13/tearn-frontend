import { useEffect, useState } from 'react';
import {
  AutoComplete,
  Pagination,
  Spin,
  Button as AntButton,
  Modal,
} from 'antd';
import Chip from 'components/build/Chip';
import Button from 'components/build/Button';
import AdminLayout from 'components/Layout/AdminLayout';
import { userRepository } from 'repository/user';
import { mutate } from 'swr';

const openNotificationWithIcon = (message, description) => {
  notification.error({
    message,
    description,
  });
};

const columns = [
  {
    id: 1,
    name: 'username',
    title: 'Username',
  },
  {
    id: 2,
    name: 'email',
    title: 'Email',
  },
  {
    id: 3,
    name: 'rol',
    title: 'Rol',
  },
  {
    id: 4,
    name: 'status',
    title: 'Status',
  },
  {
    id: 5,
    name: 'aksi',
    title: 'Aksi',
  },
];

const Dashboard = () => {
  const [data, setData] = useState([]);
  const [current, setCurrent] = useState(1);
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(true);
  const [pageSize, setPageSize] = useState(10);
  const [count, setCount] = useState(0);
  const [isModalVisible, setIsModalVisible] = useState([]);

  const { data: fetchData } = userRepository.hooks.useUser({
    page: current,
    limit: pageSize,
    search,
  });

  const handleSearch = (e) => {
    setSearch(e);
  };

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };
  const handleApprove = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await userRepository.manipulateData.approve(id, {
        status: 'diterima',
        isActive: true,
      });
      mutate(
        userRepository.url.user({
          page: current,
          limit: pageSize,
          search,
        })
      );
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    } finally {
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };
  
  const handleDissaprove = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await userRepository.manipulateData.approve(id, {
        status: 'ditolak',
        isActive: false,
      });
      mutate(
        userRepository.url.user({
          page: current,
          limit: pageSize,
          search,
        })
      );
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    } finally {
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  const handleBlock = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await userRepository.manipulateData.approve(id, {
        status: 'blok',
        isActive: false,
      });
      mutate(
        userRepository.url.user({
          page: current,
          limit: pageSize,
          search,
        })
      );
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    } finally {
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };
  const handleOpenBlock = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await userRepository.manipulateData.approve(id, {
        status: 'diterima',
        isActive: true,
      });
      mutate(
        userRepository.url.user({
          page: current,
          limit: pageSize,
          search,
        })
      );
    } catch (err) {
      if (err.response.error) {
        openNotificationWithIcon(err.response.body.message);
      }
    } finally {
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };
  const handlePagination = async (page) => {
    setCurrent(page);
  };

  useEffect(() => {
    if (fetchData !== null && fetchData !== undefined) {
      setData(fetchData.data);
      setCount(fetchData.count);
      setLoading(false);
    }
  }, [fetchData]);

  return (
    <>
      <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
        {loading ? (
          <div className='flex flex-col justify-center h-full'>
            <Spin />
          </div>
        ) : (
          <>
            <header className='flex justify-between mb-3 '>
              <h6 className='font-semibold text-2xl'>Pengguna</h6>
              <AutoComplete
                className='w-64'
                placeholder='cari pengguna'
                onChange={handleSearch}
              />
            </header>
            <div>
              <table className='w-full text-left mb-9'>
                <thead>
                  <tr className='bg-primaryLight h-10 border-b border-b-[rgba(0 0 0 0.5)] table-row'>
                    {columns.map((col) => {
                      return (
                        <th key={col.id} className='px-5 py-3 table-cell'>
                          {col.title}
                        </th>
                      );
                    })}
                  </tr>
                </thead>
                <tbody>
                  {data.map((v) => {
                    let type;
                    let action;
                    switch (v.status) {
                      case 'diterima':
                      case 'aktif':
                        type = 'active';
                        action = [
                          <AntButton
                            key='Block'
                            onClick={() => handleCancel(v.id)}
                          >
                            kembali
                          </AntButton>,
                          <AntButton
                            key='back'
                            type='primary'
                            danger
                            onClick={() => handleBlock(v.id)}
                          >
                            Blokir
                          </AntButton>,
                        ];
                        break;
                      case 'menunggu':
                        type = 'warning';
                        action = [
                          <AntButton
                            key='Block'
                            type='primary'
                            danger
                            onClick={() => handleDissaprove(v.id)}
                          >
                            Tolak
                          </AntButton>,
                          <AntButton
                            key='back'
                            type='primary'
                            onClick={() => handleApprove(v.id)}
                          >
                            Terima
                          </AntButton>,
                        ];
                        break;
                      case 'ditolak':
                      case 'tidak aktif':
                        type = 'disabled';
                        break;
                      case 'blok':
                        type = 'danger';
                        action = [
                          <AntButton
                            key='Block'
                            onClick={() => handleCancel(v.id)}
                          >
                            kembali
                          </AntButton>,
                          <AntButton
                            key='back'
                            type='primary'
                            onClick={() => handleOpenBlock(v.id)}
                          >
                            Buka blokir
                          </AntButton>,
                        ];
                        break;
                      default:
                        type = 'disabled';
                        break;
                    }

                    return (
                      <tr key={v.id}>
                        <td className='px-5 py-3 truncate max-w-[200px]'>
                          {v.username}
                        </td>
                        <td className='truncate max-w-[200px]'>{v.email}</td>
                        <td>{v.role?.role}</td>
                        <td>
                          <Chip type={type}>{v.status}</Chip>
                        </td>
                        <td>
                          <Button onClick={() => showModal(v.id)}>
                            Lihat Detail
                          </Button>
                          <Modal
                            key={v.id}
                            visible={isModalVisible[v.id]}
                            onOk={() => handleOk(v.id)}
                            onCancel={() => handleCancel(v.id)}
                            className='p-9'
                            width={600}
                            footer={action}
                          >
                            <p className='text-lg font-semibold'>Paket</p>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>
                                Username
                              </span>
                              <span className='w-1/2'>{v.username}</span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold truncate'>
                                Name lengkap
                              </span>
                              <span className='w-1/2'>
                                {v.firstName ? (
                                  v.firstName
                                ) : (
                                  <span className='text-neutral-500'>
                                    tidak memliki
                                  </span>
                                )}{' '}
                                {v.lastName ? v.lastName : ''}
                              </span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>Email</span>
                              <span className='w-1/2'>{v.email}</span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>
                                Jenis Kelamin
                              </span>
                              <span className='w-1/2'>
                                {v.gender ? (
                                  v.gender
                                ) : (
                                  <span className='text-neutral-500'>
                                    tidak memliki
                                  </span>
                                )}
                              </span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>Rol</span>
                              <span className='w-1/2'>{v.role?.role}</span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>
                                tanggal lahir
                              </span>
                              <span className='w-1/2'>
                                {v.birthDate ? (
                                  v.birthDate
                                ) : (
                                  <span className='text-neutral-500'>
                                    tidak memliki
                                  </span>
                                )}
                              </span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>
                                Nomor telepon
                              </span>
                              <span className='w-1/2'>
                                {v.phoneNumb ? (
                                  v.phoneNumb
                                ) : (
                                  <span className='text-neutral-500'>
                                    tidak memliki
                                  </span>
                                )}
                              </span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>
                                status
                              </span>
                              <span className='w-1/2'>
                                <Chip type={type}>{v.status}</Chip>
                              </span>
                            </div>
                            <div className='flex justify-between items-center mb-3'>
                              <span className='w-1/2 font-semibold'>
                                Alamat
                              </span>
                              <span className='w-1/2'>
                                {v.location ? (
                                  `${v.location.province}, ${v.location.city},
                                    ${
                                      v.location.address
                                        ? v.location.address
                                        : ''
                                    }`
                                ) : (
                                  <span className='text-neutral-500'>
                                    tidak memliki
                                  </span>
                                )}
                              </span>
                            </div>
                          </Modal>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
              <div className='flex justify-center'>
                <Pagination
                  current={current}
                  onChange={handlePagination}
                  pageSize={pageSize}
                  showTotal={(total, range) =>
                    `${range[0]}-${range[1]} of ${total} items`
                  }
                  onShowSizeChange={(current, size) => setPageSize(size)}
                  total={count}
                />
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default Dashboard;

Dashboard.getLayout = function Layout(page) {
  return <AdminLayout>{page}</AdminLayout>;
};
