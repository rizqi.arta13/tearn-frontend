import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import { Col, Form, Row, Typography, Input, notification } from 'antd';
import Button from '../components/build/Button';
import Icon from '../components/build/Icon';
import Link from 'next/link';
import { userRepository } from 'repository/user.js';
import { useRouter } from 'next/router';

const Login = observer(() => {
  //  const store = useStore();
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const openNotificationWithIcon = (type) => {
    notification[type]({
      message: 'Something not right',
      description:
        "There' something wrong check your email and password again make sure it's correct",
    });
  };

  const onFinish = async (values) => {
    setLoading(true);
    try {
      const response = await userRepository.manipulateData.login(values);
      // console.log(response);
      if (!response.serverError) {
        console.log(response.body);
        localStorage.setItem('token', response.body.token);
        router.push('/');
      }
    } catch (err) {
      if (err == 'Error: Forbidden') {
        openNotificationWithIcon('error');
      }
    }
  };

  return (
    <div
      style={{
        width: '100vw',
        display: 'flex',
        justifyContent: 'center',
        height: '100vh',
        alignItems: 'center',
      }}
    >
      <Row justify={'center'}>
        <Col>
          <div
            style={{
              display: 'flex',
              justifyContent: 'flex-start',
              marginTop: '4vh',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <div
              style={{ width: 320, textAlign: 'center' }}
              className={'shadow rounded-[20px] bg-light p-[16px]'}
            >
              <div
                className='
                        bg-primaryLight flex justify-center h-[115px] w-[115px] m-auto rounded-[2rem] items-center absolute top-[-32px] inset-x-0 border-2'
              >
                <Icon />
              </div>

              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'stretch',
                }}
                className='mt-[68px] mb-[38px]'
              >
                <Typography.Paragraph
                  style={{
                    margin: 0,
                    padding: 0,
                    fontSize: 20,
                    marginLeft: 5,
                  }}
                  className='dark font-bold'
                >
                  Teach and Learn!
                </Typography.Paragraph>
              </div>

              <Form
                layout={'vertical'}
                name='normal_login'
                className='login-form'
                onFinish={onFinish}
              >
                <Form.Item
                  style={{
                    marginBottom: '24px',
                  }}
                  name='email'
                  size={'large'}
                  rules={[
                    { required: false, message: 'Masukkan Email!' },
                    { type: 'email', message: 'Email tidak benar!' },
                  ]}
                >
                  <Input
                    type='email'
                    bordered={false}
                    placeholder='Email'
                    className='px-3 py-2 w-[256px] h-[32px] bg-primaryLight text-black border-none border-2 rounded placeholder:text-placeholder focus:outline-none'
                  />
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: '24px',
                  }}
                  name='password'
                  size={'large'}
                  rules={[
                    {
                      min: 8,
                      message: 'Gunakan 8 karakter atau lebih untuk sandi',
                    },
                    {
                      required: true,
                      message: 'Masukkan sandi anda!',
                    },
                  ]}
                >
                  <Input.Password
                    bordered={false}
                    placeholder='Password'
                    className='px-3 py-2 w-[256px] h-[32px] bg-primaryLight text-black border-none border-2 rounded placeholder:text-placeholder focus:outline-none'
                  />
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: '24px',
                    fontWeight: 'bold',
                  }}
                >
                  <Button className='w-[132px]'>Masuk</Button>
                </Form.Item>
                <div>
                  Belum punya akun? Buat{' '}
                  <Link href='/register' className='primary'>
                    disini!
                  </Link>
                </div>
              </Form>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
});

export default Login;
