export const data = [
  {
    id: 1,
    title: 'Paket UN mantap',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    price: 50000,
    rate: 4.6,
  },
  {
    id: 2,
    title: 'Paket UN mantap',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    price: 50000,
    rate: 4.6,
  },
  {
    id: 3,
    title: 'Paket UN mantap',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    price: 50000,
    rate: 4.6,
  },
  {
    id: 4,
    title: 'Paket UN mantap',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    price: 50000,
    rate: 4.6,
  },
  {
    id: 5,
    title: 'Paket UN mantap',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    price: 50000,
    rate: 4.7,
  },
  {
    id: 6,
    title: 'Paket UN mantap',
    subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
    price: 50000,
    rate: 4.6,
  },
];
