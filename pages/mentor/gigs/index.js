import {
  AutoComplete,
  Modal,
  Pagination,
  Spin,
  Button as AntButton,
  Checkbox,
  Input,
  InputNumber,
  Form,
  Empty,
} from 'antd';
import { useRouter } from 'next/router';
import Button from 'components/build/Button';
import { useEffect, useState } from 'react';
import { StarFilled } from '@ant-design/icons';
import { gigRepository } from 'repository/gigs';
import { parseJwt } from 'utils/parseJwt';
import { convertCurrency } from 'utils/formatCurrency';
import { userRepository } from 'repository/user';
import { mutate } from 'swr';

const Mentor = (props) => {
  const [form] = Form.useForm();
  const router = useRouter();
  const [data, setData] = useState([]);
  const [user, setUser] = useState({});
  const [count, setCount] = useState(0);
  const [mentor, setMentor] = useState({});
  const [current, setCurrent] = useState(1);
  const [subjects, setSubjects] = useState([]);
  const [pageSize, setPageSize] = useState(10);
  const [loading, setLoading] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState([]);
  const [editForm, setEditForm] = useState([]);

  const { data: fetchData } = gigRepository.hooks.useGigs({
    mentor: user.id,
    limit: pageSize,
    page: current,
  });

  const { data: getMentor } = userRepository.hooks.useMentorByUser(user.id);

  const { data: subjectData } = gigRepository.hooks.useSubject();

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
    setEditMode(false);
  };

  const handleCancel = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
    setEditMode(false);
  };
  const handleCreateGigs = async (data, modalId) => {
    let tempShowModal = isModalVisible.slice();
    try {
      let tempData = { ...data, mentor: mentor.id };
      await gigRepository.manipulateData.createGig(tempData);
      mutate(
        gigRepository.url.gigsByMentor({
          mentor: user.id,
          limit: pageSize,
          page: current,
        })
      );
    } catch (err) {
      console.log(err.response.body.message);
    } finally {
      form.resetFields();
      tempShowModal[modalId] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  const handleEditGigs = async (data, id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      console.log(data, id);
      let tempData = { ...data };
      await gigRepository.manipulateData.editGig(tempData, id);
      mutate(
        gigRepository.url.gigsByMentor({
          mentor: user.id,
          limit: pageSize,
          page: current,
        })
      );
    } catch (err) {
      console.log(err.response.body.message);
    } finally {
      editForm[id].resetFields();
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  const handleDeleteGigs = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await gigRepository.manipulateData.deleteGig(id);
      mutate(
        gigRepository.url.gigsByMentor({
          mentor: user.id,
          limit: pageSize,
          page: current,
        })
      );
    } catch (err) {
      console.log(err);
    }
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleSetForm = (id) => {
    let tempEditForm = editForm.slice();
    tempEditForm[id] = form;
    setEditForm(tempEditForm);
  };

  const handlePagination = async (page) => {
    setCurrent(page);
  };

  useEffect(() => {
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setUser(decodeJwt);
    }

    if (subjectData) {
      setSubjects(subjectData.data);
    }

    if (getMentor) {
      setMentor(getMentor.data);
    }

    if (fetchData) {
      setData(fetchData.data);
      setCount(fetchData.count);
      setLoading(false);
    }
  }, [fetchData, subjectData, getMentor]);

  const col = [
    'Nama Paket',
    'Paket Pelajaran',
    'Harga Per Jam',
    'Rating',
    'Aksi',
  ];

  const handleCb = (id, e) => {};
  const policyOptions = [
    {
      label: 'Bayar dimuka',
      value: 'Bayar dimuka',
    },
    {
      label: 'Transfer',
      value: 'Transfer',
    },
    {
      label: 'Pembelajaran via daring',
      value: 'Pembelajaran via daring',
    },
    {
      label: 'Pembelajaran via luring',
      value: 'Pembelajaran via luring',
    },
    {
      label: 'Pembatalan 2 jam sebelum',
      value: 'Pembatalan 2 jam sebelum',
    },
    {
      label: 'pelajaran dimulai',
      value: 'pelajaran dimulai',
    },
    {
      label: 'Hubungi sebelum membayar',
      value: 'Hubungi sebelum membayar',
    },
  ];
  const cbOptions = subjects.map((v) => {
    return { label: v.subject, value: { id: v.id } };
  });

  return (
    <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
      {loading ? (
        <div className='flex flex-col justify-center h-full'>
          <Spin />
        </div>
      ) : count <= 0 ? (
        <div className='flex flex-col justify-center h-full'>
          <Empty
            description={
              <span>Kamu belum punya paket ayo buat paket sekarang</span>
            }
          >
            <Button className='w-64' onClick={() => showModal('ffa')}>
              Buat paket baru
            </Button>
            <Modal
              title='Tambah Paket Pembelajaran'
              visible={isModalVisible['ffa']}
              cancelText='Kembali'
              onCancel={() => handleCancel('ffa')}
              okText='Tambah'
              onOk={() =>
                form
                  .validateFields()
                  .then((values) => handleCreateGigs(values))
                  .catch((err) => {
                    console.log(err);
                  })
              }
              className='p-9'
              width={780}
            >
              <Form form={form} preserve={false}>
                <Form.Item name='mentor'>
                  <Input hidden value={mentor.id} />
                </Form.Item>
                <div className='flex gap-6'>
                  <div className='w-1/3 flex flex-col'>
                    <span className='text-lg font-semibold'>
                      Mata Pelajaran
                    </span>
                    <Form.Item
                      name='subject'
                      required
                      rules={[
                        {
                          required: true,
                          message:
                            'Jangan lupa untuk memlih mata pelajaran yang diajar',
                        },
                      ]}
                    >
                      <Checkbox.Group
                        options={cbOptions}
                        onChange={(e) => handleCb('ffa', e)}
                      />
                    </Form.Item>
                  </div>
                  <div className='w-1/3 flex flex-col'>
                    <span className='text-lg font-semibold'>Nama Paket</span>
                    <Form.Item
                      name='title'
                      placeholder=''
                      rules={[
                        {
                          required: true,
                          message: 'Judul paket tidak boleh kosong',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <span className='text-lg font-semibold'>Harga Paket</span>
                    <Form.Item
                      name='amount'
                      required
                      rules={[
                        {
                          required: true,
                          message: 'Harga paket tidak boleh kosong',
                        },
                        {
                          type: 'number',
                          message: 'Harga paket harus dalam bentuk angka',
                        },
                      ]}
                    >
                      <InputNumber
                        min={10000}
                        placeholder='Ex: 100000'
                        style={{ width: '100%' }}
                      />
                    </Form.Item>

                    <span className='text-lg font-semibold'>
                      Deskripsi Paket
                    </span>
                    <Form.Item name='description'>
                      <Input.TextArea autoSize={{ minRows: 3, maxRows: 5 }} />
                    </Form.Item>
                  </div>
                  <div className='w-1/3'>
                    <span className='text-lg font-semibold'>Ketentuan</span>
                    <Form.Item name='policy'>
                      <Checkbox.Group
                        options={policyOptions}
                        onChange={(e) => handleCb('ffa', e)}
                      />
                    </Form.Item>
                  </div>
                </div>
              </Form>
            </Modal>
          </Empty>
        </div>
      ) : (
        <>
          <header className='flex justify-between mb-3 '>
            <h6 className='font-semibold text-2xl'>Paket Pemebelajaran</h6>
            <Button className='w-64' onClick={() => showModal('ffa')}>
              Buat paket baru
            </Button>
            <Modal
              title='Tambah Paket Pembelajaran'
              visible={isModalVisible['ffa']}
              cancelText='Kembali'
              onCancel={() => handleCancel('ffa')}
              okText='Tambah'
              onOk={() =>
                form
                  .validateFields()
                  .then((values) => handleCreateGigs(values))
                  .catch((err) => {
                    console.log(err);
                  })
              }
              className='p-9'
              width={780}
            >
              <Form form={form} preserve={false}>
                <Form.Item name='mentor'>
                  <Input hidden value={mentor.id} />
                </Form.Item>
                <div className='flex gap-6'>
                  <div className='w-1/3 flex flex-col'>
                    <span className='text-lg font-semibold'>
                      Mata Pelajaran
                    </span>
                    <Form.Item
                      name='subject'
                      required
                      rules={[
                        {
                          required: true,
                          message:
                            'Jangan lupa untuk memlih mata pelajaran yang diajar',
                        },
                      ]}
                    >
                      <Checkbox.Group
                        options={cbOptions}
                        onChange={(e) => handleCb('ffa', e)}
                      />
                    </Form.Item>
                  </div>
                  <div className='w-1/3 flex flex-col'>
                    <span className='text-lg font-semibold'>Nama Paket</span>
                    <Form.Item
                      name='title'
                      placeholder=''
                      rules={[
                        {
                          required: true,
                          message: 'Judul paket tidak boleh kosong',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                    <span className='text-lg font-semibold'>Harga Paket</span>
                    <Form.Item
                      name='amount'
                      required
                      rules={[
                        {
                          required: true,
                          message: 'Harga paket tidak boleh kosong',
                        },
                        {
                          type: 'number',
                          message: 'Harga paket harus dalam bentuk angka',
                        },
                      ]}
                    >
                      <InputNumber
                        min={10000}
                        placeholder='Ex: 100000'
                        style={{ width: '100%' }}
                      />
                    </Form.Item>

                    <span className='text-lg font-semibold'>
                      Deskripsi Paket
                    </span>
                    <Form.Item name='description'>
                      <Input.TextArea autoSize={{ minRows: 3, maxRows: 5 }} />
                    </Form.Item>
                  </div>
                  <div className='w-1/3'>
                    <span className='text-lg font-semibold'>Ketentuan</span>
                    <Form.Item name='policy'>
                      <Checkbox.Group
                        options={policyOptions}
                        onChange={(e) => handleCb('ffa', e)}
                      />
                    </Form.Item>
                  </div>
                </div>
              </Form>
            </Modal>
          </header>
          <div>
            <table className='w-full text-left mb-9'>
              <thead>
                <tr className='bg-primaryLight h-10 border-b border-b-[rgba(0 0 0 0.5)] table-row'>
                  {col.map((v, i) => {
                    return (
                      <th key={i} className='px-5 py-3 table-cell'>
                        {v}
                      </th>
                    );
                  })}
                </tr>
              </thead>
              <tbody>
                {data.map((v) => {
                  return (
                    <tr
                      key={v.id}
                      className='pb-3 h-10 border-b border-b-[rgba(0 0 0 0.5)]'
                    >
                      <td className='px-5 py-3'>{v.title}</td>
                      <td className='px-5 py-3'>
                        <ul>
                          {v.subjects.map((v, i) => {
                            return <li key={i}>{v.subjectId?.subject}</li>;
                          })}
                        </ul>
                      </td>
                      <td className='px-5 py-3'>
                        {convertCurrency(v.amount)}
                        <sub>/jam</sub>
                      </td>
                      <td className='px-5 py-3 flex items-center gap-1'>
                        ({v.rate ? v.rate : 0.0})
                        <StarFilled className='text-yellow-400' />
                      </td>
                      <td className='pl-5'>
                        <Button
                          onClick={() => {
                            showModal(v.id);
                            handleSetForm(v.id);
                          }}
                        >
                          Lihat
                        </Button>
                        <Modal
                          title='Detail Paket Pembelajaran'
                          key={v.id}
                          visible={isModalVisible[v.id]}
                          onOk={() => handleOk(v.id)}
                          onCancel={() => handleCancel(v.id)}
                          className='p-9'
                          width={780}
                          footer={
                            editMode
                              ? [
                                  <AntButton
                                    key='cancel'
                                    danger
                                    onClick={() => setEditMode(false)}
                                  >
                                    batal
                                  </AntButton>,
                                  <AntButton
                                    key='edit'
                                    htmlType='submit'
                                    onClick={() => {
                                      editForm[v.id]
                                        .validateFields()
                                        .then((values) =>
                                          handleEditGigs(values, v.id)
                                        )
                                        .catch((err) => {
                                          console.log(err);
                                        });
                                    }}
                                    type='primary'
                                  >
                                    ubah
                                  </AntButton>,
                                ]
                              : [
                                  <AntButton
                                    key='Block'
                                    danger
                                    onClick={() => handleDeleteGigs(v.id)}
                                  >
                                    hapus
                                  </AntButton>,

                                  <AntButton
                                    key='back'
                                    onClick={() => setEditMode(true)}
                                    type='primary'
                                  >
                                    ubah
                                  </AntButton>,
                                ]
                          }
                        >
                          <Form
                            disabled={!editMode}
                            form={editForm[v.id]}
                            initialValues={{
                              title: v.title,
                              description: v.description,
                              amount: v.amount,
                              subject: v.subjects.map((v) => {
                                console.log(v.subjectId?.id);
                                return v.subjectId?.id;
                              }),
                            }}
                          >
                            <div className='flex gap-6'>
                              <div className='w-1/3 flex flex-col'>
                                <span className='text-lg font-semibold'>
                                  Mata Pelajaran
                                </span>
                                <Form.Item name='subject'>
                                  {console.log(
                                    v.subjects.map((v) => {
                                      return v.subjectId?.id;
                                    })
                                  )}
                                  <Checkbox.Group
                                    options={cbOptions}
                                    onChange={(e) => handleCb(v.id, e)}
                                  />
                                </Form.Item>
                              </div>
                              <div className='w-1/3 flex flex-col'>
                                <span className='text-lg font-semibold'>
                                  Nama Paket
                                </span>
                                <Form.Item name='title'>
                                  <Input />
                                </Form.Item>
                                <span className='text-lg font-semibold'>
                                  Harga Paket
                                </span>
                                <Form.Item name='amount'>
                                  <InputNumber style={{ width: '100%' }} />
                                </Form.Item>

                                <span className='text-lg font-semibold'>
                                  Deskripsi Paket
                                </span>
                                <Form.Item name='description'>
                                  <Input.TextArea
                                    autoSize={{ minRows: 3, maxRows: 5 }}
                                  />
                                </Form.Item>
                              </div>
                              <div className='w-1/3'>
                                <span className='text-lg font-semibold'>
                                  Ketentuan
                                </span>
                                <Form.Item>
                                  <Checkbox.Group
                                    defaultValue={['Aljabar']}
                                    options={policyOptions}
                                    onChange={(e) => handleCb(v.id, e)}
                                  />
                                </Form.Item>
                              </div>
                            </div>
                          </Form>
                        </Modal>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div className='flex justify-center'>
              <Pagination
                current={current}
                onChange={handlePagination}
                pageSize={pageSize}
                showTotal={(total, range) =>
                  `${range[0]}-${range[1]} of ${total} items`
                }
                onShowSizeChange={(current, size) => setPageSize(size)}
                total={count}
              />
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Mentor;
