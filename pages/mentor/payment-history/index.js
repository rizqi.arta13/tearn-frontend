import {
  Avatar,
  Modal,
  Spin,
  Button as AntButton,
  Image,
  Empty,
  Pagination,
} from 'antd';
import Chip from 'components/build/Chip';
import Button from 'components/build/Button';
import { data } from './data';
import { useContext, useEffect, useState } from 'react';
import { format } from 'date-fns';
import { transactionRepository } from 'repository/transaction';
import { UserMentorContext } from 'components/Layout/MentorLayout';
import { convertCurrency } from 'utils/formatCurrency';

const Payment = () => {
  const user = useContext(UserMentorContext);
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [loading, setLoading] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState([]);

  const { data: fetchData } = transactionRepository.hooks.usePaymentByMentor({
    mentor: user.id,
    page: current,
    limit: pageSize,
    include: 'selesai',
    exclude: '',
  });
  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handlePagination = async (page) => {
    setCurrent(page);
  };

  useEffect(() => {
    setTimeout(() => {
      try {
        if (fetchData) {
          setData(fetchData.data);
          setCount(fetchData.count);
        }
      } finally {
        setLoading(false);
      }
    }, 500);
  }, [fetchData]);

  return (
    <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
      {loading ? (
        <div className='flex flex-col justify-center h-full'>
          <Spin />
        </div>
      ) : count <= 0 ? (
        <div className='flex flex-col justify-center h-full'>
          <Empty />
        </div>
      ) : (
        <>
          <header className='flex justify-between mb-3 '>
            <h6 className='font-semibold text-2xl'>Daftar Pemesanan</h6>
          </header>
          {data.map((v) => {
            let type;

            switch (v.status) {
              case 'berjalan':
                type = 'primary';
                break;
              case 'selesai':
              case 'sudah dibayar':
                type = 'active';
                break;
              case 'menunggu pembayaran':
                type = 'warning';
                break;
              case 'belum dibayar':
                type = 'danger';
                break;
              default:
                type = 'disabled';
                break;
            }
            return (
              <div
                key={v.id}
                className='w-full flex flex-row flex-wrap justify-between shadow-md rounded-xl p-4 mb-6'
              >
                <span className='flex'>
                  <Avatar
                    size={80}
                    src={v.order.user.profilePhoto}
                    className='bg-orange-500'
                  >
                    {v.order.user.profilePhoto
                      ? ''
                      : v.order.user.username.charAt(0).toUpperCase()}
                  </Avatar>
                  <div className='ml-7'>
                    <p className='m-0 text-lg font-medium text-primary'>
                      {v.order.gig.title}
                    </p>
                    <p className='m-0 text-sm'>{v.order.user.username}</p>
                    <div className='flex gap-7'>
                      <p className='m-0 min-w-[100px]'>Estimasi Hari</p>
                      <p className='m-0'>
                        {format(new Date(v.order.startDate), 'dd/LL/yyyy')} -{' '}
                        {format(new Date(v.order.endDate), 'dd/LL/yyyy')}
                      </p>
                    </div>
                    <div className='flex gap-7'>
                      <p className='m-0 min-w-[100px]'>Estimasi Jam</p>
                      <p className='m-0'>
                        {format(new Date(v.order.startHour), 'HH:mm')} -{' '}
                        {format(new Date(v.order.endHour), 'HH:mm')}
                      </p>
                    </div>
                  </div>
                </span>
                <div className='flex flex-col justify-between'>
                  <p className='text-lg m-0 self-end'>
                    {convertCurrency(v.order.amount)}
                    <sub>total</sub>
                  </p>
                  <Button onClick={() => showModal(v.id)}>Lihat</Button>
                  <Modal
                    key={v.id}
                    visible={isModalVisible[v.id]}
                    onOk={() => handleOk(v.id)}
                    onCancel={() => handleCancel(v.id)}
                    className='p-9'
                    width={820}
                    footer={[
                      <AntButton key='back' onClick={() => handleCancel(v.id)}>
                        Kembali
                      </AntButton>,
                      <AntButton
                        key='Block'
                        type='primary'
                        primary
                        href={process.env.NEXT_PUBLIC_BASE_URL + '/pdf/' + v.id}
                      >
                        Unduh
                      </AntButton>,
                    ]}
                  >
                    <div className='flex gap-6'>
                      <div className='w-1/2'>
                        <p className='text-lg font-semibold'>Paket</p>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>
                            Nama pemesan
                          </span>
                          <span className='w-1/2'>{v.order.user.username}</span>
                        </div>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>
                            Tanggal selesai
                          </span>
                          <span className='w-1/2'>
                            {format(
                              new Date(v.finishedAt),
                              'HH:mm E dd/LL/yyyy'
                            )}
                          </span>
                        </div>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>
                            Tanggal terjadwal
                          </span>
                          <span className='w-1/2'>
                            {format(new Date(v.order.startDate), 'dd/LL/yyyy')}{' '}
                            - {format(new Date(v.order.endDate), 'dd/LL/yyyy')}
                          </span>
                        </div>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>
                            Jam terjadwal
                          </span>
                          <span className='w-1/2'>
                            {format(new Date(v.order.startHour), 'HH:mm')} -{' '}
                            {format(new Date(v.order.endHour), 'HH:mm')}
                          </span>
                        </div>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>status</span>
                          <span className='w-1/2'>
                            <Chip type={type}>{v.status}</Chip>
                          </span>
                        </div>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>
                            Nama paket
                          </span>
                          <span className='w-1/2'>{v.order.gig.title}</span>
                        </div>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>
                            Mata Pelajaran
                          </span>
                          <ul className='w-1/2'>
                            {v.order.gig.subjects.map((v, i) => {
                              return <li key={i}>{v.subjectId.subject}</li>;
                            })}
                          </ul>
                        </div>
                        <div className='flex justify-between items-center mb-3'>
                          <span className='w-1/2 font-semibold'>Harga</span>
                          <span className='w-1/2 text-primary font-medium'>
                            {convertCurrency(v.order.amount)}
                          </span>
                        </div>
                      </div>
                      <div className='w-1/2'>
                        <div className='font-semibold'>Bukti pembayaran</div>
                        <Image
                          width={200}
                          src='/img/dummy/proof-payment-dummy.png'
                          alt='prrof of payment'
                        />
                      </div>
                    </div>
                  </Modal>
                </div>
              </div>
            );
          })}
        </>
      )}
      <Pagination
        current={current}
        onChange={handlePagination}
        pageSize={pageSize}
        showTotal={(total, range) =>
          `${range[0]}-${range[1]} of ${total} items`
        }
        onShowSizeChange={(current, size) => setPageSize(size)}
        total={count}
      />
    </div>
  );
};

export default Payment;
