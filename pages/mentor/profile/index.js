import { LoadingOutlined } from '@ant-design/icons';
import { location } from './location';
import { Avatar, Button, Form, message, Upload } from 'antd';
import InputLogged from 'components/build/InputLogged';
import Select from 'components/build/Select';
import ClientLayout from 'components/Layout/ClientLayout';
import { useEffect, useState } from 'react';

const MentorProfile = (props) => {
  const [prov, setProv] = useState('');
  const [loading, setLoading] = useState(true);

  const handleSelectChange = (e) => {
    setProv(e);
  };

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 500);
  }, []);

  const propsUpload = {
    name: 'file',
    action: '',
    headers: {
      authorization: 'authorization-text',
    },

    onChange(info) {
      if (info.file.status !== 'uploading') {
        console.log(info.file, info.fileList);
      }

      if (info.file.status === 'done') {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
      {loading ? (
        <div className='w-full h-full flex items-center justify-center animate-pulse text-primary'>
          <LoadingOutlined
            style={{ fontSize: '48px' }}
            className='animate-spin text-primary'
          />
        </div>
      ) : (
        <>
          <div className='bg-white max-w-5xl rounded'>
            <header className='flex justify-between m-0'>
              <h6 className='font-medium text-[18px]'>
                Pengaturan Profil Akun
              </h6>
            </header>
            <div className='m-0 w-full h-[1px] bg-gradient-to-r from-[#00000050] to-transparent'></div>
            <div className='mt-8 '>
              <Form
                action='#'
                id='user_profile'
                className=' flex flex-row gap-8'
              >
                <div className='flex flex-col w-2/3 gap-4'>
                  <div className=''>
                    <label className='font-medium text-[14px]' for='username'>
                      Username
                    </label>
                    <InputLogged
                      type='text'
                      id='username'
                      placeholder='Username'
                    />
                  </div>
                  <div className='flex flex-row gap-4'>
                    <div className='w-1/2'>
                      <label
                        className='font-medium text-[14px]'
                        for='firstName'
                      >
                        Nama Depan
                      </label>
                      <InputLogged
                        type='text'
                        id='firstName'
                        placeholder='nama depan'
                      />
                    </div>
                    <div className='w-1/2'>
                      <label className='font-medium text-[14px]' for='lastName'>
                        Nama Belakang
                      </label>
                      <InputLogged
                        type='text'
                        id='lastName'
                        placeholder='nama belakang'
                      />
                    </div>
                  </div>
                  <div className=''>
                    <label className='font-medium text-[14px]' for='email'>
                      Email
                    </label>
                    <InputLogged type='text' id='email' placeholder='email' />
                  </div>
                  <div className=''>
                    <label className='font-medium text-[14px]' for='phone'>
                      Nomor Telepon
                    </label>
                    <InputLogged
                      type='text'
                      id='phone'
                      placeholder='080808080808'
                    />
                  </div>
                  <div className=''>
                    <label className='font-medium text-[14px]' for='gender'>
                      Gender
                    </label>
                    <Select form='user_profile' id='gender' name='genders'>
                      <option value='' selected disabled hidden>
                        pilih gender
                      </option>
                      <option value='pria'>pria</option>
                      <option value='wanita'>wanita</option>
                    </Select>
                  </div>
                  <div className=''>
                    <label className='font-medium text-[14px]' for='birthdate'>
                      Tanggal Lahir
                    </label>
                    <InputLogged
                      type='date'
                      id='birthdate'
                      placeholder='mm/dd/yy'
                    />
                  </div>
                  <div className='flex flex-row gap-4'>
                    <div className='w-1/2'>
                      <label className='font-medium text-[14px]' for='province'>
                        provinsi
                      </label>
                      <Select
                        form='user_profile'
                        id='province'
                        name='province'
                        onChange={(e) => handleSelectChange(e.target.value)}
                      >
                        <option value='' selected disabled hidden>
                          pilih provinsi
                        </option>
                        {location.map((v, i) => {
                          return (
                            <>
                              <option key={i} value={v.provinsi}>
                                {v.provinsi}
                              </option>
                            </>
                          );
                        })}
                      </Select>
                    </div>
                    <div className='w-1/2'>
                      <label className='font-medium text-[14px]' for='city'>
                        Kota / Kabupaten
                      </label>
                      <Select form='user_profile' id='city' name='city'>
                        <option value='' selected disabled hidden>
                          pilih kota / kabupaten
                        </option>

                        {location
                          .find((item) => item.provinsi === prov)
                          .kota.map((kot, i) => {
                            return (
                              <option key={i} value={kot}>
                                {kot}
                              </option>
                            );
                          })}
                      </Select>
                    </div>
                  </div>
                  <div className='flex flex-row gap-4'>
                    <div className='w-1/2'>
                      <label className='font-medium text-[14px]' for='district'>
                        kecamatan
                      </label>
                      <Select form='user_profile' id='district' name='district'>
                        <option value='' selected disabled hidden>
                          pilih kecamatan
                        </option>

                        {location
                          .find((item) => item.provinsi === prov)
                          .kota.map((kot, i) => {
                            return (
                              <option key={i} value={kot}>
                                {kot}
                              </option>
                            );
                          })}
                      </Select>
                    </div>
                    <div className='w-1/2'>
                      <label className='font-medium text-[14px]' for='address'>
                        Alamat
                      </label>
                      <InputLogged
                        type='text'
                        id='address'
                        placeholder=''
                        className='h-[72px]'
                      />
                    </div>
                  </div>
                  <div className='mt-2'>
                    <input
                      type='submit'
                      value='SIMPAN'
                      className='bg-primary w-full text-light font-medium h-[36px] rounded cursor-pointer'
                    />
                  </div>
                </div>
                <div className='flex flex-col gap-4 w-1/3'>
                  <label className='font-medium text-[14px]' for='img-profile'>
                    Foto Profil
                  </label>
                  <div className='w-[calc(80%)]'>
                    <Avatar
                      src={'/img/avatar/man1.jpeg'}
                      className='w-full h-full'
                    />
                  </div>
                  {/* <input type="file" className="bg-primary w-full text-light font-medium h-[36px] rounded cursor-pointer"/> */}
                  <div className='w-full'>
                    <Upload
                      listType='picture'
                      className='w-full'
                      {...propsUpload}
                    >
                      <button
                        className='px-20 h-[32px] bg-primary text-light rounded'
                        block
                      >
                        Pilih Foto Profil
                      </button>
                    </Upload>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default MentorProfile;
