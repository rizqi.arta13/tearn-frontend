import {
  Avatar,
  Modal,
  Spin,
  Button as AntButton,
  Empty,
  Pagination,
} from 'antd';
import Chip from 'components/build/Chip';
import Button from 'components/build/Button';
import { data } from './data';
import { useContext, useEffect, useState } from 'react';
import { format } from 'date-fns';
import { transactionRepository } from 'repository/transaction';
import { UserMentorContext } from 'components/Layout/MentorLayout';
import { convertCurrency } from 'utils/formatCurrency';
import Link from 'next/link';
import { mutate } from 'swr';

const Payment = () => {
  const user = useContext(UserMentorContext);
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [loading, setLoading] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState([]);

  const { data: fetchData } = transactionRepository.hooks.usePaymentByMentor({
    mentor: user.id,
    page: current,
    limit: pageSize,
    exclude: 'selesai',
    include: '',
  });

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleClose = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = async (id) => {
    let tempShowModal = isModalVisible.slice();

    try {
      await transactionRepository.manipulateData.approve(id, {
        status: 'selesai',
      });
    } catch (err) {
      console.log(err);
    } finally {
      mutate(
        transactionRepository.url.getPaymentByGigMentor({
          mentor: user.id,
          page: current,
          limit: pageSize,
          exclude: 'selesai',
          include: '',
        })
      );
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  const handlePagination = async (page) => {
    setCurrent(page);
  };

  useEffect(() => {
    if (fetchData) {
      setData(fetchData.data);
      setCount(fetchData.count);
      setLoading(false);
    }
  }, [fetchData]);

  return (
    <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
      {loading ? (
        <div className='flex flex-col justify-center h-full'>
          <Spin />
        </div>
      ) : count <= 0 ? (
        <div className='flex flex-col justify-center h-full'>
          <Empty />
        </div>
      ) : (
        <>
          <header className='flex justify-between mb-3 '>
            <h6 className='font-semibold text-2xl'>Daftar Pemesanan</h6>
          </header>
          {data.map((v) => {
            let type;
            let action;
            let msg;

            switch (v.status) {
              case 'berjalan':
                type = 'primary';
                action = [
                  <AntButton key='Block' onClick={() => handleClose(v.id)}>
                    kembali
                  </AntButton>,
                  <AntButton
                    key='back'
                    type='primary'
                    danger
                    onClick={() => handleCancel(v.id)}
                  >
                    Batal
                  </AntButton>,
                ];
                break;
              case 'sudah dibayar':
                type = 'active';
                msg = (
                  <>
                    uang belum masuk? laporkan ke admin{' '}
                    <Link href='https://wa.me/1234567890?text=Halo admin tearn pesanan saya belum terbayar'>
                      1234567890
                    </Link>
                  </>
                );
                action = [
                  <AntButton key='Block' onClick={() => handleClose(v.id)}>
                    kembali
                  </AntButton>,
                  <AntButton
                    key='back'
                    type='primary'
                    primary
                    onClick={() => handleCancel(v.id)}
                  >
                    Selesai
                  </AntButton>,
                ];
                break;
              case 'menunggu':
                type = 'warning';
                action = [
                  <AntButton key='back' onClick={() => handleClose(v.id)}>
                    Kembali
                  </AntButton>,
                  <AntButton
                    key='Ok'
                    type='primary'
                    onClick={() => handleOk(v.id)}
                  >
                    Ok
                  </AntButton>,
                ];
                break;
              case 'belum dibayar':
                type = 'danger';
                action = [
                  <AntButton key='back' onClick={() => handleClose(v.id)}>
                    Kembali
                  </AntButton>,
                  <AntButton
                    key='Ok'
                    type='primary'
                    onClick={() => handleOk(v.id)}
                  >
                    Ok
                  </AntButton>,
                ];
                break;
              default:
                type = 'disabled';
                break;
            }
            return (
              <div
                key={v.id}
                className='w-full flex flex-row flex-wrap justify-between shadow-md rounded-xl p-4 mb-6'
              >
                <span className='flex'>
                  <Avatar
                    size={80}
                    src={v.order.user.profilePhoto}
                    className='bg-orange-500'
                  >
                    {v.order.user.profilePhoto
                      ? ''
                      : v.order.user.username.charAt(0).toUpperCase()}
                  </Avatar>
                  <div className='ml-7'>
                    <p className='m-0 text-lg font-medium text-primary'>
                      {v.order.gig.title}
                    </p>
                    <p className='mb-5 text-sm'>{v.order.user.username}</p>
                    <Chip type={type}>{v.status}</Chip>
                  </div>
                </span>
                <div className='flex flex-col justify-between'>
                  <p className='text-lg m-0 self-end'>
                    {new Intl.NumberFormat('id-ID', {
                      style: 'currency',
                      currency: 'IDR',
                    }).format(v.order.amount)}{' '}
                    <sub>total</sub>
                  </p>
                  <Button onClick={() => showModal(v.id)}>Lihat Detail</Button>
                  <Modal
                    key={v.id}
                    visible={isModalVisible[v.id]}
                    onOk={() => handleOk(v.id)}
                    onCancel={() => handleCancel(v.id)}
                    className='p-9'
                    width={600}
                    footer={action}
                  >
                    <p className='text-lg font-semibold'>Paket</p>
                    <div className='flex justify-between items-center mb-3'>
                      <span className='w-1/2 font-semibold'>Nama pemesan</span>
                      <span className='w-1/2'>{v.order.user.username}</span>
                    </div>
                    <div className='flex justify-between items-center mb-3'>
                      <span className='w-1/2 font-semibold'>
                        Tanggal terjadwal
                      </span>
                      <span className='w-1/2'>
                        {format(new Date(v.order.startDate), 'dd/LL/yyyy')} -{' '}
                        {format(new Date(v.order.endDate), 'dd/LL/yyyy')}
                      </span>
                    </div>
                    <div className='flex justify-between items-center mb-3'>
                      <span className='w-1/2 font-semibold'>Jam terjadwal</span>
                      <span className='w-1/2'>
                        {format(new Date(v.order.startHour), 'HH:mm')} -{' '}
                        {format(new Date(v.order.endHour), 'HH:mm')}
                      </span>
                    </div>
                    <div className='flex justify-between items-center mb-3'>
                      <span className='w-1/2 font-semibold'>status</span>
                      <span className='w-1/2'>
                        <Chip type={type}>{v.status}</Chip>
                      </span>
                    </div>
                    <div className='flex justify-between items-center mb-3'>
                      <span className='w-1/2 font-semibold'>Nama paket</span>
                      <span className='w-1/2'>{v.order.gig.title}</span>
                    </div>
                    <div className='flex justify-between items-center mb-3'>
                      <span className='w-1/2 font-semibold'>
                        Mata Pelajaran
                      </span>
                      <ul className='w-1/2'>
                        {v.order.gig.subjects.map((v, i) => {
                          return <li key={i}>{v.subjectId.subject}</li>;
                        })}
                      </ul>
                    </div>
                    <div className='flex justify-between items-center mb-3'>
                      <span className='w-1/2 font-semibold'>Harga</span>
                      <span className='w-1/2 text-primary font-medium'>
                        {convertCurrency(v.order.amount)}
                      </span>
                    </div>
                    {msg ? msg : ''}
                  </Modal>
                </div>
              </div>
            );
          })}
        </>
      )}
      <Pagination
        current={current}
        onChange={handlePagination}
        pageSize={pageSize}
        showTotal={(total, range) =>
          `${range[0]}-${range[1]} of ${total} items`
        }
        onShowSizeChange={(current, size) => setPageSize(size)}
        total={count}
      />
    </div>
  );
};

export default Payment;
