export const data = [
  {
    id: 1,
    gigs: {
      id: 1,
      title: 'Paket UN mantap',
      subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
      price: 50000,
      rate: 4.6,
    },
    user: {
      username: 'bodybody',
    },
    status: 'menunggu pembayaran',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
  },
  {
    id: 2,
    gigs: {
      id: 1,
      title: 'Paket UN mantap',
      subjects: ['Bahasa Indonesia', 'Bahasa Inggris', 'Sejarah'],
      price: 50000,
      rate: 4.6,
    },
    user: {
      username: 'arthentic',
      profile: 'https://thispersondoesnotexist.com/image',
    },
    status: 'belum dibayar',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
  },
  {
    id: 3,
    gigs: {
      id: 2,
      title: 'Makin dekat dengan yesus',
      subjects: ['1001 alkitab', 'Sejarah nasrani'],
      price: 50000,
      rate: 4.6,
    },
    user: {
      username: 'GapingDragon07',
    },
    status: 'sudah dibayar',
    startDate: '2022-08-12',
    endDate: '2022-08-15',
    startHour: '2014-01-01 11:00:00.000 +0700',
    endHour: '2014-01-01 15:00:00.000 +0700',
  },
];
