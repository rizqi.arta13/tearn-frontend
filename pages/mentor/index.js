import {
  AutoComplete,
  Pagination,
  Spin,
  Button as AntButton,
  Modal,
  Empty,
} from 'antd';
import Chip from 'components/build/Chip';
import { useRouter } from 'next/router';
import Button from 'components/build/Button';
import { useEffect, useState } from 'react';
import MentorLayout from 'components/Layout/MentorLayout';
import { transactionRepository } from 'repository/transaction';
import { parseJwt } from 'utils/parseJwt';
import { format } from 'date-fns';
import Link from 'next/link';
import { mutate } from 'swr';

const Mentor = (props) => {
  const [user, setUser] = useState({});
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [current, setCurrent] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState([]);

  const { data: fetchData } = transactionRepository.hooks.useOrderByMentor({
    mentor: user.id,
    page: current,
    limit: pageSize,
    search,
  });

  const handleSearch = (e) => {
    setSearch(e);
  };

  const showModal = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = true;
    setIsModalVisible(tempShowModal);
  };

  const handleOk = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleClose = (id) => {
    let tempShowModal = isModalVisible.slice();
    tempShowModal[id] = false;
    setIsModalVisible(tempShowModal);
  };

  const handleCancel = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await transactionRepository.manipulateData.updateOrderStatus(id, {
        status: 'dibatalkan',
      });
    } catch (err) {
      console.log(err);
    } finally {
      mutate(
        transactionRepository.url.getOrderByMentor({
          mentor: user.id,
          page: current,
          limit: pageSize,
          search,
        })
      );
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  const handleFinish = async (id) => {
    let tempShowModal = isModalVisible.slice();
    try {
      await transactionRepository.manipulateData.updateOrderStatus(id, {
        status: 'selesai',
      });
    } catch (err) {
      console.log(err);
    } finally {
      mutate(
        transactionRepository.url.getOrderByMentor({
          mentor: user.id,
          page: current,
          limit: pageSize,
          search,
        })
      );
      tempShowModal[id] = false;
      setIsModalVisible(tempShowModal);
    }
  };

  const handlePagination = async (page) => {
    setCurrent(page);
  };

  useEffect(() => {
    setLoading(true);
    let token;
    if (typeof window !== 'undefined') {
      token = localStorage.getItem('token');
    }

    const decodeJwt = parseJwt(token);
    if (decodeJwt) {
      setUser(decodeJwt);
    }

    if (fetchData) {
      console.log(fetchData);
      setData(fetchData.data);
      setCount(fetchData.count);
      setLoading(false);
    }
  }, [fetchData]);

  const col = [
    'Klien',
    'Paket Pelajaran',
    'Estimasi Waktu',
    'Estimasi Jam',
    'Status',
    'Aksi',
  ];

  return (
    <div className='bg-white max-w-4xl w-2/3 rounded shadow-md p-6'>
      <header className='flex justify-between mb-3 '>
        <h6 className='font-semibold text-2xl'>Klienku</h6>
        <AutoComplete
          className='w-64'
          placeholder='cari pengguna'
          onChange={handleSearch}
        />
      </header>
      {loading ? (
        <div className='flex flex-col justify-center h-full'>
          <Spin />
        </div>
      ) : count <= 0 ? (
        <div className='flex flex-col justify-center h-full'>
          <Empty />
        </div>
      ) : (
        <>
          <div>
            <table className='w-full text-left mb-9'>
              <thead>
                <tr className='bg-primaryLight h-10 border-b border-b-[rgba(0 0 0 0.5)] table-row'>
                  {col.map((v, i) => {
                    return (
                      <th key={i} className='px-5 py-3 table-cell'>
                        {v}
                      </th>
                    );
                  })}
                </tr>
              </thead>
              <tbody>
                {data.map((v) => {
                  let type;
                  let action;

                  switch (v.status) {
                    case 'berlangsung':
                    case 'ongoing':
                      type = 'primary';
                      action = [
                        <AntButton key='back' onClick={() => handleClose(v.id)}>
                          Kembali
                        </AntButton>,
                        <AntButton
                          key='Ok'
                          type='primary'
                          danger
                          onClick={() => handleCancel(v.id)}
                        >
                          Batal
                        </AntButton>,
                        <AntButton
                          key='Ok'
                          type='primary'
                          primary
                          onClick={() => handleFinish(v.id)}
                        >
                          Selesai
                        </AntButton>,
                      ];
                      break;
                    case 'finished':
                    case 'selesai':
                      type = 'active';
                      action = [
                        <AntButton key='back' onClick={() => handleClose(v.id)}>
                          Kembali
                        </AntButton>,
                        <AntButton
                          key='Ok'
                          type='primary'
                          onClick={() => handleOk(v.id)}
                        >
                          Ok
                        </AntButton>,
                      ];
                      break;
                    case 'waiting for payment':
                    case 'menunggu':
                      type = 'warning';
                      action = [
                        <AntButton key='back' onClick={() => handleClose(v.id)}>
                          Kembali
                        </AntButton>,
                        <AntButton
                          key='Ok'
                          type='primary'
                          danger
                          onClick={() => handleCancel(v.id)}
                        >
                          Batal
                        </AntButton>,
                      ];
                      break;
                    case 'canceled':
                    case 'dibatalkan':
                      type = 'danger';
                      action = [
                        <AntButton key='back' onClick={() => handleClose(v.id)}>
                          Kembali
                        </AntButton>,
                        <AntButton
                          key='Ok'
                          type='primary'
                          onClick={() => handleOk(v.id)}
                        >
                          Ok
                        </AntButton>,
                      ];
                      break;
                    default:
                      type = 'disabled';
                      break;
                  }

                  return (
                    <tr
                      key={v.id}
                      className='pb-3 h-10 border-b border-b-[rgba(0 0 0 0.5)]'
                    >
                      <td className='px-5 py-3'>{v.user.username}</td>
                      <td className='px-5 py-3'>
                        <ul>
                          {v.gig.mentor.availability.map((v, i) => {
                            return <li key={i}>{v.day}</li>;
                          })}
                        </ul>
                      </td>
                      <td className='px-5 py-3'>
                        {format(new Date(v.startDate), 'dd/LL/yy')} -{' '}
                        {format(new Date(v.endDate), 'dd/LL/yy')}
                      </td>
                      <td className='px-5 py-3'>
                        {format(new Date(v.startHour), 'HH:mm')} -{' '}
                        {format(new Date(v.endHour), 'HH:mm')}
                      </td>
                      <td className='px-5 py-3'>
                        <Chip type={type}>{v.status}</Chip>
                      </td>
                      <td>
                        <Button onClick={() => showModal(v.id)}>Lihat</Button>
                        <Modal
                          key={v.id}
                          visible={isModalVisible[v.id]}
                          onOk={() => handleOk(v.id)}
                          onCancel={() => handleClose(v.id)}
                          className='p-9'
                          width={600}
                          footer={action}
                        >
                          <p className='text-lg font-semibold'>Paket</p>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>
                              Nama pemesan
                            </span>
                            <span className='w-1/2'>{v.user.username}</span>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>
                              No telepon pemesan
                            </span>
                            <span className='w-1/2'>
                              <Link
                                target='_blank'
                                href={`https://wa.me/${v.user.phoneNumb}?text=halo saya ${user.firstName} dari tearn`}
                                passHref
                              >
                                <a>{v.user.phoneNumb}</a>
                              </Link>
                            </span>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>Jadwal</span>
                            <ul className='w-1/2'>
                              {v.gig.mentor.availability.map((v, i) => {
                                return <li key={i}>{v.day}</li>;
                              })}
                            </ul>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>
                              Tanggal terjadwal
                            </span>
                            <span className='w-1/2'>
                              {format(new Date(v.startDate), 'dd/LL/yy')} -{' '}
                              {format(new Date(v.endDate), 'dd/LL/yy')}
                            </span>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>
                              Jam terjadwal
                            </span>
                            <span className='w-1/2'>
                              {format(new Date(v.startHour), 'HH:mm')} -{' '}
                              {format(new Date(v.endHour), 'HH:mm')}
                            </span>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>status</span>
                            <span className='w-1/2'>
                              <Chip type={type}>{v.status}</Chip>
                            </span>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>
                              Nama paket
                            </span>
                            <span className='w-1/2'>{v.gig.title}</span>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>
                              Mata Pelajaran
                            </span>
                            <ul className='w-1/2'>
                              {v.gig.subjects.map((v, i) => {
                                return <li key={i}>{v.subjectId?.subject}</li>;
                              })}
                            </ul>
                          </div>
                          <div className='flex justify-between items-center mb-3'>
                            <span className='w-1/2 font-semibold'>Harga</span>
                            <span className='w-1/2'>
                              {new Intl.NumberFormat('id-ID', {
                                style: 'currency',
                                currency: 'IDR',
                              }).format(v.amount)}
                              <sub>/hari</sub>
                            </span>
                          </div>
                        </Modal>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <div className='flex justify-center'>
              <Pagination
                current={current}
                onChange={handlePagination}
                pageSize={pageSize}
                showTotal={(total, range) =>
                  `${range[0]}-${range[1]} of ${total} items`
                }
                onShowSizeChange={(current, size) => setPageSize(size)}
                total={count}
              />
            </div>
          </div>
        </>
      )}
    </div>
  );
};

export default Mentor;

Mentor.getLayout = function Layout(page) {
  return <MentorLayout>{page}</MentorLayout>;
};
