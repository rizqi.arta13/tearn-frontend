import { Avatar, Divider, Rate } from 'antd';
import Button from 'components/build/Button';
import Link from 'next/link';

const Test = () => {
  return (
    <>
      <div className='w-full py-8 px-14 flex gap-10'>
        <div className='flex flex-col p-8 bg-light rounded-md w-full gap-8'>
          <h1 className='text-[18px]'>Detail Jasa</h1>
          <div className='flex gap-4'>
            <div className='flex items-center'>
              <Avatar size={128} src={''} className='bg-primaryTrans'>
                S
              </Avatar>
            </div>
            <div className='w-full flex flex-col justify-center'>
              <p className='font-medium text-[14px] m-0'>Jarjrit Sighn</p>
              <h3 className='leading-[32px] text-primary text-[28px] m-0'>Paket Berjaya Bersama Jarjit Singh</h3>
              <div className='m-0 flex mt-1'>
                <Avatar className='w-[16px] h-[16px] mr-1' src={'/img/icons/star.svg'} shape='square' />
                <p className='font-medium m-0 text-[12px]'>
                  4.6{' '}
                  <Link href={'/#'}>
                    <a>(20 penilaian)</a>
                  </Link>
                </p>
              </div>
            </div>
          </div>
          <div className='flex flex-col mb-6 w-full'>
            <h1 className='text-[16px]'>Tentang Jasa Ini</h1>
            <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />
            <div className='flex flex-row pt-8 gap-4'>
              <div className='w-1/3 font-medium'>Deskripsi</div>
              <div className='w-full'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ac id magna leo purus malesuada enim ipsum
                consequat. At tellus id aliquam nibh ipsum. Lacus nisl sed pellentesque turpis. Nulla sagittis non
                elementum augue.
              </div>
            </div>
            <div className='flex flex-row pt-8 gap-4'>
              <div className='w-1/3 font-medium'>Ketentuan</div>
              <ul className='w-full list-disc ml-6'>
                <li>Hubungi sebelum membayar</li>
                <li>Bayar dimuka</li>
                <li>Mentoring via daring</li>
                <li>Pembatalan pembelajaran dilakukan 2 jam sebelum jam pelajaran dimulai</li>
              </ul>
            </div>
            <div className='flex flex-row pt-8  gap-4'>
              <div className='w-1/3 font-medium'>Kontak</div>
              <ul className='w-full'>
                <li>Hubungi sebelum membayar</li>
                <li>Bayar dimuka</li>
                <li>Mentoring via daring</li>
                <li>Pembatalan pembelajaran dilakukan 2 jam sebelum jam pelajaran dimulai</li>
              </ul>
            </div>
            <div className='flex flex-row pt-8  gap-4'>
              <div className='w-1/3 font-medium'>Ketersediaan</div>
              <ul className='w-full list-disc ml-6'>
                <li>Hubungi sebelum membayar</li>
                <li>Bayar dimuka</li>
                <li>Mentoring via daring</li>
                <li>Pembatalan pembelajaran dilakukan 2 jam sebelum jam pelajaran dimulai</li>
              </ul>
            </div>
            <div className='flex flex-row pt-8  gap-4'>
              <div className='w-1/3 font-medium'>Mata Pelajaran</div>
              <ul className='w-full list-disc ml-6'>
                <li>Hubungi sebelum membayar</li>
                <li>Bayar dimuka</li>
                <li>Mentoring via daring</li>
                <li>Pembatalan pembelajaran dilakukan 2 jam sebelum jam pelajaran dimulai</li>
              </ul>
            </div>
          </div>
          <div className='flex flex-col w-full'>
            <h1 className='text-[16px] m-0'>Penilaian</h1>
            <div className='flex items-center gap-4'>
              <Rate disabled allowHalf defaultValue={4.5} />
              <p className='m-0'>
                <span className='font-semibold'>4.7 </span>(dari 20 ulasan)
              </p>
            </div>
            <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />

            <div className='w-full flex pt-6'>
              <div className='w-5/12 flex gap-4'>
                <div className='flex items-center'>
                  <Avatar size={40} src={''} className='bg-primaryTrans'>
                    A
                  </Avatar>
                </div>
                <div className='w-full flex flex-col justify-center'>
                  <p className='font-medium m-0 mb-1 text-[12px] leading-[12px]'>Alexis Messi</p>
                  <p className='m-0 text-[12px] leading-[12px]'>12 November 2022</p>
                </div>
              </div>
              <div className='w-full'>
                <Rate disabled allowHalf defaultValue={4.5} className='h-[16px]' />
              </div>
            </div>
          </div>
        </div>
        <div className='w-2/6 h-min sticky top-6'>
          <div className='w-full bg-light rounded-md shadow-lg flex flex-col'>
            <p className='m-0 p-3'>
              <span className='font-medium text-[18px]'>Rp 50.000 </span> /jam
            </p>
            <Divider className='w-full h-[2px] m-0 bg-[#00000020]' />
            <p className='mr-4 ml-4 mb-0 mt-6 text-[12px] font-semibold'>Ketentuan</p>
            <ul className='ml-8 mr-4 list-disc text-ellipsis leading-loose text-justify'>
              <li>Bayar dimuka</li>
              <li>Mentoring via daring</li>
              <li>pembatalan dilakukan 2 jam sebelum jam pelajaran dimulai</li>
            </ul>
            <div className='p-4'>
              <Button className='w-full h-[40px] m-auto text-[16px] font-semibold'>Rekrut</Button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Test;
