import { http } from '../utils/http';
import useSWR from 'swr';

const url = {
  createPayment: () => `/payment/create`,
  payment: ({ page, limit }) => `/payment?limit=${limit}&page=${page}`,
  uploadProofPaymet: () => `/file/upload`,
  approval: (payment) => `/payment/${payment}/update`,
  order: ({ page, limit, search }) =>
    `/order?page=${page}&pageSize=${limit}&search=${search}`,
  orderHistory: ({ page, limit }) =>
    `/order?page=${page}&pageSize=${limit}&search=selesai`,
  orderPayment: ({ page, limit }) =>
    `/order?page=${page}&pageSize=${limit}&search=menunggu pembayaran`,
  orderUpdate: (id) => `/order/${id}`,
  getOrderById: (id) => `/order/${id}`,
  getPaymentByGigMentor: ({ mentor, limit, page, exclude, include }) =>
    `/payment/${mentor}/mentor?limit=${limit}&page=${page}&include=${include}&exclude=${exclude}`,
  getOrderByMentor: ({ mentor, page, limit, search }) =>
    `/order/${mentor}/mentor?limit=${limit}&page=${page}&search=${search}`,
};

const hooks = {
  usePayment(filter) {
    return useSWR(url.payment(filter), http.fetcher);
  },

  useOrder(filter) {
    return useSWR(url.order(filter), http.fetcher);
  },
  useOrderHistory(filter) {
    return useSWR(url.orderHistory(filter), http.fetcher);
  },
  useOrderPayment(filter) {
    return useSWR(url.orderPayment(filter), http.fetcher);
  },
  usePaymentByMentor(filter) {
    return useSWR(url.getPaymentByGigMentor(filter), http.fetcher);
  },
  useOrderByMentor(filter) {
    return useSWR(url.getOrderByMentor(filter), http.fetcher);
  },
};

const manipulateData = {
  createPayment(data) {
    return http.post(url.createPayment()).send(data);
  },
  approve(payment, { status, isActive }) {
    return http.put(url.approval(payment)).send({ status, isActive });
  },
  orderUpdate(data, { status }) {
    return http.put(url.orderUpdate(data)).send({ status });
  },
  uploadProofPaymet(file) {
    // console.log(file, 'apa isi datanya 2');
    const formData = new FormData();
    formData.append('file', file);
    return http.post(url.uploadProofPaymet()).send(formData);
  },
  updateOrderStatus(id, { status }) {
    return http.put(url.getOrderById(id)).send({ status });
  },
};

export const transactionRepository = {
  url,
  manipulateData,
  hooks,
};
