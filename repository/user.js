import { http } from '../utils/http';
import useSWR from 'swr';

const url = {
  user: ({ page, limit, search }) =>
    `/users?page=${page}&limit=${limit}&search=${search}`,
  register: () => `/auth/register`,
  login: () => '/auth/login',
  mentorByUser: (user) => `/users/${user}/mentor`,
  approval: (user) => `/users/${user}/approve`,
};

const hooks = {
  useUser(filter) {
    return useSWR(url.user(filter), http.fetcher);
  },
  useMentorByUser(user) {
    return useSWR(url.mentorByUser(user), http.fetcher);
  },
};

const manipulateData = {
  register(data) {
    return http.post(url.register()).send(data);
  },
  login(data) {
    return http.post(url.login()).send(data);
  },
  approve(user, { status, isActive }) {
    return http.put(url.approval(user)).send({ status, isActive });
  },
};

export const userRepository = {
  url,
  manipulateData,
  hooks,
};
