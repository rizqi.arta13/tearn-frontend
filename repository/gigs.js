import { http } from '../utils/http';
import useSWR from 'swr';

const url = {
  gigCreate: () => `/gig/create`,
  gigsByMentor: ({ mentor, limit, page }) =>
    `/gig/${mentor}/mentor?limit=${limit}&offset=${page}`,
  getSubjects: () => `/subject/`,
  deleteSubjects: (id) => `/subject/${id}`,
  gigUpdate: (id) => `/gig/${id}/update`,
  gigDelete: (id) => `/gig/${id}`,
  gigSearch: ({
    startAge,
    endAge,
    startDate,
    endDate,
    endPrice,
    endTime,
    gender,
    startPrice,
    startTime,
    subject,
    educationLevel,
    pages,
    limit,
  }) =>
    `/gig/search/gig?gender=${gender}&startAge=${startAge}&endAge=${endAge}&startDate=${startDate}&endDate=${endDate}&startTime=${startTime}&endTime=${endTime}&startPrice=${startPrice}&endPrice=${endPrice}&subject=${subject}&pages=${pages}&limit=${limit}&educationLevel=${educationLevel}`,
  gigDetail: (id) => `/gig/${id}`,
};

const hooks = {
  useGigs(filter) {
    return useSWR(url.gigsByMentor(filter), http.fetcher);
  },
  useSubject() {
    return useSWR(url.getSubjects(), http.fetcher);
  },
  useSearchGigs(filter) {
    return useSWR(url.gigSearch(filter), http.fetcher);
  },
  useGigDetail(id) {
    return useSWR(url.gigDetail(id), http.fetcher);
  },
};

const manipulateData = {
  createSubject(data) {
    return http.post(url.getSubjects()).send(data);
  },

  createGig(data) {
    return http.post(url.gigCreate()).send(data);
  },

  editGig(data, id) {
    return http.post(url.gigUpdate(id)).send(data);
  },

  deleteGig(id) {
    return http.del(url.gigDelete(id)).send();
  },

  deleteSubject(id) {
    return http.del(url.deleteSubjects(id)).send();
  },
};

export const gigRepository = {
  url,
  manipulateData,
  hooks,
};
