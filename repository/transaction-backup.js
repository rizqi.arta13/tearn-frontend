import { http } from '../utils/http';
import useSWR from 'swr';

const url = {
  getOrderByMentor: ({ mentor, page, limit, search }) =>
    `/order/${mentor}/mentor?limit=${limit}&page=${page}&search=${search}`,
  payment: ({ page, limit }) => `/payment?limit=${limit}&page=${page}`,
  approval: (payment) => `/payment/${payment}/update`,
  getOrderById: (id) => `/order/${id}`,
  getPaymentByGigMentor: ({ mentor, limit, page, exclude, include }) =>
    `/payment/${mentor}/mentor?limit=${limit}&page=${page}&include=${include}&exclude=${exclude}`,
};

const hooks = {
  usePayment(filter) {
    return useSWR(url.payment(filter), http.fetcher);
  },
  usePaymentByMentor(filter) {
    return useSWR(url.getPaymentByGigMentor(filter), http.fetcher);
  },
  useOrderByMentor(filter) {
    return useSWR(url.getOrderByMentor(filter), http.fetcher);
  },
};

const manipulateData = {
  approve(payment, { status, isActive }) {
    return http.put(url.approval(payment)).send({ status, isActive });
  },
  updateOrderStatus(id, { status }) {
    return http.put(url.getOrderById(id)).send({ status });
  },
};

export const transactionRepository = {
  url,
  manipulateData,
  hooks,
};
